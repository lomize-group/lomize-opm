namespace :import do

	task assemblies: :environment do
		# this code just kind of wings it because there's no reasonable re-use for it.
		# the file it parses (db/assemble_table.txt) is a very unique format.

		# assembl_table2 is newer data
		filename = "assembl_table2.txt"
		filepath = File.join Rails.root, "db", filename

		puts "Importing assembly data from #{filepath}"

		total_counter = 0
		data = File.read(filepath).split(/\n\s*\n/)
		data.each do |subunit|
			asm = Assembly.new
			lines = subunit.split("\n")


			# 1st line (many fields)
			# arbitrary amount of whitespace is usually leading and trailing.
			# don't need the resolution number because it is recached from primary_structures.
			### FORMAT: pdbid|subunitcode|resolution|name|uniprotcode|
			line0_format = ["pdbid", "protein_letter", "resolution", "name", "uniprotcode"]
			curr = 0
			line = lines[curr].split("|")
			line0_format.each_with_index do |key, idx|
				# strip extraneous leading and trailing whitespace.
				# some fields have intentional spaces in them that should be maintainted.
				asm[key] = line[idx].strip()
			end
			curr += 1


			# 2nd line (amino acid sequence)
			# no whitespace.
			### FORMAT: aminoAcidSequenceWithNoWhiteSpace
			line = lines[curr].gsub(/\s/, "")
			asm.aaseq = line
			curr += 1


			# 3rd line (transmembrane segments)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: #OfTransmembraneSegments|(startRes1-endRes1),(startRes2-endRes2),...
			line = lines[curr].gsub(/\s/, "").split("|")
			transmem_segments = nil
			if line.size > 1
				transmem_segments = line[1].split(",").select{|elt| elt.size > 1}
				transmem_segments.map!.with_index{|segment, idx| (idx+1).to_s + segment}
				transmem_segments = transmem_segments.join(",")
			end
			asm.transmembrane_segment = transmem_segments
			asm.transmembrane_segment_count = line[0].to_i
			curr += 1


			# 4th line (transmembrane α-helices)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: #OfTransmembraneAlphaHelices|(startRes1-endRes1),(startRes2-endRes2),...
			line = lines[curr].gsub(/\s/, "").split("|")
			transmem_a_helices = nil
			if line.size > 1
				transmem_a_helices = line[1].split(",").select{|elt| elt.size > 1}
				transmem_a_helices.map!.with_index{|segment, idx| (idx+1).to_s + segment}
				transmem_a_helices = transmem_a_helices.join(",")
			end
			asm.transmembrane_alpha_helix = transmem_a_helices
			asm.transmembrane_alpha_helix_count = line[0].to_i
			curr += 1


			# 5th line (other α-helices)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: #OfOtherAlphaHelices|(startRes1-endRes1),(startRes2-endRes2),...
			line = lines[curr].gsub(/\s/, "").split("|")
			other_a_helices = nil
			if line.size > 1
				other_a_helices = line[1].split(",").select{|elt| elt.size > 1}
				other_a_helices.map!.with_index{|segment, idx| (idx+1).to_s + segment}
				other_a_helices = other_a_helices.join(",")
			end
			asm.other_alpha_helix = other_a_helices
			asm.other_alpha_helix_count = line[0].to_i
			curr += 1


			# 6th line (β-strands)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: #OfBetaStrands|(startRes1-endRes1),(startRes2-endRes2),...
			line = lines[curr].gsub(/\s/, "").split("|")
			b_strands = nil
			segment_count = 0
			if line.count > 1
				segment_count = line[0].to_i
				b_strands = line[1].split(",").select{|elt| elt.size > 1}
				while b_strands.size < segment_count
					curr += 1
					b_strands.concat(lines[curr].gsub(/\s/, "").split(",").select{|elt| elt.size > 1})
				end
				b_strands.map!.with_index{|segment, idx| (idx+1).to_s + segment}
				b_strands = b_strands.join(",")
			end
			asm.beta_strand = b_strands
			asm.beta_strand_count = line[0].to_i
			curr += 1

			
			# 7th line (disordered segments)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: #OfDisorderedSegments|(startRes1-endRes1),(startRes2-endRes2),...
			# occasionally it is formatted such that some of the sequences appear on a newline...		
			line = lines[curr].gsub(/\s/, "").split("|")
			disordered_segments = nil
			segment_count = 0
			if line.size > 1
				segment_count = line[0].to_i
				disordered_segments = line[1].split(",").select{|elt| elt.size > 1}
				disordered_segments.map!.with_index{|segment, idx| (idx+1).to_s + segment}
				disordered_segments = disordered_segments.join(",")
			end
			asm.disordered_segment = disordered_segments
			asm.disordered_segment_count = segment_count
			curr += 1


			# 8th line to 2nd-to-last line (interacting pairs of TM α-helices)
			# arbitrary whitespace throughout, can be removed.
			### FORMAT: TMHelix1<whitespace>TMHelix2<whitespace>associationFreeEnergies
			helix_interactions = []
			while curr < lines.size - 1
				line = lines[curr].strip().split(/\s+/)
				helix_interactions.push(line.join(","))
				curr += 1
			end
			asm.helix_interaction = helix_interactions.join(";")


			# last line (assembly equation)
			# arbitrary whitespace throughout, can be removed.
			### EXAMPLE: ((((1+2)+3)+(4+5))+(6+7))
			asm.equation = lines[-1].strip()
			asm.update_hierarchy
			if !asm.primary_structure.nil? && !asm.structure_subunit.nil?
				asm.save!
				asm.family.update_count
				asm.membrane.update_count
				total_counter += 1
				if total_counter % 100 == 0
					puts "importing - #{total_counter} completed..."
				end
			end
		end
		puts "Done, total of #{total_counter} assemblies imported."
	end
end



