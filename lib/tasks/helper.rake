namespace :helper do
	module DataHelper
		class Counter
			def initialize(count_start = 0)
				@current_count = count_start
			end

			def print_increment(incr_amount=1, every_n=1, suffix="records done")
				@current_count += incr_amount
				if @current_count % every_n == 0
					puts "@@@ #{@current_count.to_s} #{suffix}"
				end
			end

			def count
				@current_count
			end
		end

		def self.parse_json(data)
			require 'json'
			begin
				return JSON.parse(data)
			rescue JSON::ParserError
				return nil
			end
		end

		def self.get_json_data(url, params = nil)
			require 'net/http'
			require 'json'

			uri = URI(url)
			if !params.nil?
				uri.query = URI.encode_www_form(params)
			end
			res = Net::HTTP.get_response(uri)
            if res.code == "200"
              begin
                  data = JSON.parse(res.body)
              rescue JSON::ParserError
                  data = nil
              end
            else
              data = nil
            end
			return data
		end

		def self.get_xml_data(url, params = nil)
			require 'open-uri'
			require 'nokogiri'

			uri = URI(url)
			if !params.nil?
				uri.query = URI.encode_www_form(params)
			end
			data = Nokogiri::XML(open(uri))
			return data
		end
	end
end
