namespace :data do
    namespace :update do
        concurrency = 15

        def create_request pool, url
            require 'typhoeus'
            request = Typhoeus::Request.new(url)
            request.on_complete do |response|
                if response.success?
                    yield ActiveSupport::JSON.decode(response.body)
                else
                    # if we do happen to still be requesting too fast, wait more
                    # and re-send the request synchronously.
                    # Response code of 0 might sometimes happen if connection fluctuates.
                    # Response code 429 indicates that we have sent requests too fast.
                    # The RCSB API sometimes responds with 503 if it has a lot of traffic.
                    if response.code == 0 || response.code == 429 || response.code == 503
                        sleep(3)
                        puts "Retrying #{url}"
                        response = Typhoeus.get(url)
                        if response.code == 200
                            yield ActiveSupport::JSON.decode(response.body)
                        else
                            # Nothing you can really do about it if it fails again
                            puts "ERROR SENDING REQUEST TO URL #{url}"
                            puts "Failed with status code #{response.code}"
                        end
                    else
                        # Nothing you can really do about failures
                        puts "ERROR SENDING REQUEST TO URL #{url}"
                        puts "Failed with status code #{response.code}"
                    end
                end
            end

            return request
        end

        task update_resolutions: :environment do
            pool = Typhoeus::Hydra.new(max_concurrency: concurrency)

            rcsb_pdb_url = "https://data.rcsb.org/rest/v1/core/entry/"

            # Get small batches and request concurrently.
            # Doing thousands of requests, 1 or more per primary_structure, synchronously takes literal hours.
            # Adding some concurrency reduces the time dramatically.


            def update_resolution obj, data
                resMethodMap = {
                    'X-RAY DIFFRACTION' => nil,
                    'ELECTRON MICROSCOPY' => "EM",
                    'ELECTRON CRYSTALLOGRAPHY' => "EC",
                    'NEUTRON DIFFRACTION' => "NC",
                    'FIBER DIFFRACTION' => "FD",
                    'SOLUTION NMR' => "NMR"
                }
                begin
                    method = data["exptl"][0]["method"]
                    method = resMethodMap.fetch(method, "N/A")
                rescue
                    method = "N/A"
                end

                case method
                when nil, "EM", "EC", "NC", "FD"
                    begin
                        resolution = data["rcsb_entry_info"]["resolution_combined"][0]
                        resolution = resolution.round(2).to_s
                    rescue
                        resolution = "N/A"
                    else
                        unless method.nil?
                            resolution = resolution + " " + method
                        end
                    end
                when "NMR"
                    resolution = "NMR"
                else
                    resolution = "N/A"
                end

                # save resolution to object
                obj.resolution = resolution
                obj.save!
            end

            ctr = 0

            puts "Updating resolution for SecondaryRepresentation"
            SecondaryRepresentation.find_in_batches(:batch_size => concurrency) do |batch|
                batch.each do |obj|
                    request = create_request(pool, rcsb_pdb_url + obj.pdbid) do |data|
                        update_resolution(obj, data)
                    end

                    pool.queue(request)
                end

                pool.run # start running queued requests concurrently

                ctr += batch.size
                puts "Updating resolution for SecondaryRepresentation... #{ctr} done"

                sleep(4) # sleep a few seconds to avoid HTTP 429 "too many requests"
            end
        end

        task primary_structure_uniprotcode: :environment do
            pool = Typhoeus::Hydra.new(max_concurrency: concurrency)
            pool_mutex = Mutex.new
            ENTRY_URL = "https://data.rcsb.org/rest/v1/core/entry"
            UNIPROT_URL = "https://data.rcsb.org/rest/v1/core/uniprot"
            counter = 0
            PrimaryStructure.find_in_batches(:batch_size => concurrency) do |batch|
                proteins = Hash.new
                proteins_mutex = Mutex.new
                uniprot_codes = Hash.new { |h, id| h[id] = Set[] }
                uniprot_codes_mutex = Mutex.new

                batch.each do |protein|
                    proteins_mutex.synchronize do
                        proteins[protein.id] = protein
                    end

                    pdb = protein.pdbid
                    url = "#{ENTRY_URL}/#{pdb}"
                    request = create_request(pool, url) do |entry_data|
                        entry_data = DataHelper::get_json_data(url)
                        begin
                            entity_ids = entry_data["rcsb_entry_container_identifiers"]["polymer_entity_ids"]
                        rescue
                            entity_ids = []
                        end

                        for entity_id in entity_ids do
                            url = "#{UNIPROT_URL}/#{pdb}/#{entity_id}"
                            # create_request will only return non-nil data
                            request = create_request(pool, url) do |uniprot_entry_data|
                                for uniprot_data in uniprot_entry_data do
                                    begin
                                        uniprot_code = uniprot_data["rcsb_uniprot_entry_name"][0]
                                    rescue
                                        # nothing
                                    else
                                        uniprot_codes_mutex.synchronize do
                                            uniprot_codes[protein.id].add(uniprot_code)
                                        end
                                    end
                                end
                            end

                            pool_mutex.synchronize do
                                pool.queue(request)
                            end
                        end
                    end

                    pool_mutex.synchronize do
                        pool.queue(request)
                    end
                end

                pool.run

                uniprot_codes.each do |protein_id, uniprot_codes|
                    protein = proteins[protein_id]
                    uniprot_codes = uniprot_codes.sort.join(" ")

                    # skip if would completely erase entry
                    if uniprot_codes.empty? and !protein.uniprotcode.nil?
                        puts "Skip erase #{protein.pdbid} uniprot"
                    # skip if no new entry
                    elsif uniprot_codes == protein.uniprotcode
                        #puts "Skip no new entry #{protein.pdbid} uniprot"
                    # only update if new entry
                    else
                        protein.uniprotcode = uniprot_codes
                        protein.updated_at = Time.now
                        protein.save!
                    end
                end

                counter += batch.size
                puts "Updating uniprotcodes for primary structures: #{counter} done"
                sleep(4)
            end
        end
    end
end
