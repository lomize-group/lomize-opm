namespace :data do

	desc "pulls the production data from heroku"
	task :pull do
	  app = " lomize-group-opm"
	  heroku = "/usr/local/bin/heroku"

	  drop_db = "rake db:drop"
	  set_env = "rake db:environment:set"

	  # cmd = "heroku git:remote -a" + app
	  # system cmd

	  remote_db = "DATABASE_URL"
	  local_db = "lomize-opm_development"
	  pull = "heroku pg:pull " + remote_db + " " + local_db + " --app " + app
	  cmd = drop_db + " && " + pull + " && " + set_env
	  puts cmd
	  system cmd
	end

	# desc "rebuilds the data from migrations"
	# task :rebuild do
		
	# 	drop_db = "rake db:drop"
	# 	create_db = "rake db:create"
	# 	migrate_db = "rake db:migrate"
	# 	seed_db = "rake db:seed"
	# 	hierarchy_cache = "rake data:update_hierarchy"
	# 	standardize_numbers = "rake data:renumber"

	# 	cmd = drop_db+' && '+create_db+' && '+migrate_db +' && '+seed_db +' && '+ hierarchy_cache +' && '+ standardize_numbers 
	# 	puts cmd
	# 	system cmd
	# end

	task :backup_production do
		puts "============================================"
		puts "============================================"
		puts "BACKING UP & DOWNLOADING PRODUCTION DATABASE"
		puts "(saving as lastest.dump)"
		puts "============================================"
		puts "============================================"

		app = 'lomize-group-opm'
		heroku = '/usr/local/bin/heroku'

		# save backup postgres dump locally just in case
		# this is also saved for a limited time in heroku
		cmd = 'rm -f latest.dump'
        puts cmd
		system cmd
		cmd = 'heroku pg:backups:capture --app '+app
        puts cmd
		system cmd
		cmd = 'heroku pg:backups:download'
        puts cmd
		system cmd
	end

	desc "Renumbers data per new ordering"
	task renumber: :environment do
		
		puts "Renumbering Species"
		new_num = 0.0
		objects = Species.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

		puts "Renumbering Membrane"
		new_num = 0.0
		objects = Membrane.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

		puts "Renumbering Citation"
		new_num = 0.0
		objects = Citation.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end


		puts "Renumbering Type"
		new_num = 0.0
		objects = Type.order("ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end
		
		puts "Renumbering Classtype"
		new_num = 0.0
		objects = Classtype.joins(:type).order("types.ordering, ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

		puts "Renumbering Superfamily"
		new_num = 0.0
		objects = Superfamily.joins(:classtype).order("classtypes.ordering, ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

		puts "Renumbering Family"
		new_num = 0.0
		objects = Family.joins(:superfamily).order("superfamilies.ordering, ordering")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

		puts "Renumbering PrimaryStructure"
		new_num = 0.0
		objects = PrimaryStructure.joins(:family).order("families.ordering, name")
		for object in objects do
			new_num += 1
			object.update_column("ordering", new_num)
		end

	end


	desc "Remakes all hierarchy cached data"
	task update_hierarchy: :environment do
		
		count = Type.all.count
		puts "Updating Cache for #{count} Types" 
		Type.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Type.first
		
		count = Classtype.all.count
		puts "Updating Cache for #{count} Classtype"
		Classtype.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Classtype.first

		count = Superfamily.all.count
		puts "Updating Cache for #{count} Superfamilies"
		Superfamily.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Superfamily.first

		count = Family.all.count
		puts "Updating Cache for #{count} Families"
		Family.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Family.first

		count = PrimaryStructure.all.count
		puts "Updating Cache for #{count} PrimaryStructures"
		PrimaryStructure.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> PrimaryStructure.first

		count = 0
		puts "Updating Cache for #{count} SecondaryRepresentations"
		SecondaryRepresentation.find_each(:batch_size => 1000) do |object|
		  # object.??
		end
		puts :json=> SecondaryRepresentation.first

		count = 0
		puts "Updating Cache for #{count} StructureSubunits"
		StructureSubunit.find_each(:batch_size => 1000) do |object|
		  # object.??
		end
		puts :json=> StructureSubunit.first

		count = Species.all.count
		puts "Updating Cache for #{count} Species"
		Species.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Species.first

		count = Membrane.all.count
		puts "Updating Cache for #{count} Membranes"
		Membrane.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Membrane.first

		count = 0
		puts "Updating Cache for #{count} Citations"
		Citation.find_each(:batch_size => 1000) do |object|
		  # object.??
		end
		puts :json=> Citation.first

		count = Assembly.all.count
		puts "Updating Cache for #{count} Assemblies"
		Assembly.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Assembly.first

		count = Lipid.all.count
		puts "Updating Cache for #{count} Lipid"
		Lipid.find_each(:batch_size => 1000) do |object|
		  object.update_hierarchy
		end
		puts :json=> Lipid.first
	end

	desc "Remakes all hierarchy cached data"
	task update_counts: :environment do
		puts "Updating count for Membranes"
		Membrane.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for Species"
		Species.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for Type"
		Type.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for Classtype"
		Classtype.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for Superfamily"
		Superfamily.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for Family"
		Family.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

		puts "Updating count for PrimaryStructure"
		PrimaryStructure.find_each(:batch_size => 1000) do |object|
		  object.update_count
		end

	end

	desc "Updates the constants such as date last updated"
	task update_constants: :environment do
		puts "Updating most recent change date"

		# check all tables for most recent dates
		# get most recent date and format it to MonthName DD, YYYY
		key = "lastUpdated"
		new_date = [
			Citation.order("updated_at").last.updated_at,
			Membrane.order("updated_at").last.updated_at,
			Species.order("updated_at").last.updated_at,
			Type.order("updated_at").last.updated_at,
			Classtype.order("updated_at").last.updated_at,
			Superfamily.order("updated_at").last.updated_at,
			Family.order("updated_at").last.updated_at,
			PrimaryStructure.order("updated_at").last.updated_at,
			StructureSubunit.order("updated_at").last.updated_at,
			SecondaryRepresentation.order("updated_at").last.updated_at
		].max.strftime("%B %d, %Y")

		Constant.put(key, new_date)
	end


	desc "aggregator for update_hierarchy and renumber"
	task recache: :environment do
		Rake::Task["data:update_hierarchy"].invoke
		Rake::Task["data:renumber"].invoke
		Rake::Task["data:update_counts"].invoke
		Rake::Task["data:update_constants"].invoke
	end

end
