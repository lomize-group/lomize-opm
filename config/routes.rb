Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # these routes are explicitly defined for ease of reading
  # this should really be nested if routes beyond simple gets are implemented
  # nesting should never be more than one level deep

  match "types" => 'types#find_all', :via => :get
  match "types/:id" => 'types#find_one', :via => :get
  match "types/:id/classtypes" => 'types#find_classtypes_of', :via => :get
  match "types/:id/primary_structures" => 'types#find_primary_structures_of', :via => :get

  match "classtypes" => 'classtypes#find_all', :via => :get
  match "classtypes/:id" => 'classtypes#find_one', :via => :get
  match "classtypes/:id/superfamilies" => 'classtypes#find_superfamilies_of', :via => :get
  match "classtypes/:id/primary_structures" => 'classtypes#find_primary_structures_of', :via => :get

  match "superfamilies" => 'superfamilies#find_all', :via => :get
  match "superfamilies/:id" => 'superfamilies#find_one', :via => :get
  match "superfamilies/:id/families" => 'superfamilies#find_families_of', :via => :get
  match "superfamilies/:id/primary_structures" => 'superfamilies#find_primary_structures_of', :via => :get

  match "families" => 'families#find_all', :via => :get
  match "families/pfamcode/:pfamcode" => 'families#find_by_pfam', :via => :get
  match "families/:id" => 'families#find_one', :via => :get
  match "families/:id/primary_structures" => 'families#find_primary_structures_of', :via => :get

  match "primary_structures" => 'primary_structures#find_all', :via => :get
  match "primary_structures/random" => 'primary_structures#find_random', :via => :get
  match "primary_structures/advanced_search" => 'primary_structures#advanced_search', :via => :get

  match "primary_structures/pdbid/:pdbid" => 'primary_structures#find_one_by_pdbid', :via => :get
  match "primary_structures/uniprotcode/:uniprotcode" => 'primary_structures#find_by_uniprotcode', :via => :get
  match "primary_structures/pfamcode/:pfamcode" => 'primary_structures#find_by_pfam', :via => :get
  match "primary_structures/pdb_file/:pdbid" => 'primary_structures#get_pdb_file', :via => :get

  match "primary_structures/:id" => 'primary_structures#find_one', :via => :get
  match "primary_structures/:id/membrane_lipids" => 'primary_structures#find_membrane_lipids', :via => :get
  match "all_representations" => 'primary_structures#find_all_representations', :via => :get
  match "pdbids" => 'primary_structures#find_all_pdbids', :via => :get

  match "secondary_representations" => 'secondary_representations#find_all', :via => :get

  match "structure_subunits" => 'structure_subunits#find_all', :via => :get
  match "structure_subunits/pdbid/:pdbid" => 'structure_subunits#find_by_pdbid', :via => :get
  match "structure_subunits/:id" => 'structure_subunits#find_one', :via => :get


  ## peripheral objects

  match "citations" => 'citations#find_all', :via => :get
  match "citations/:id" => 'citations#find_one', :via => :get

  match "membranes" => 'membranes#find_all', :via => :get
  match "membranes/:id" => 'membranes#find_one', :via => :get
  match "membranes/:id/primary_structures" => 'membranes#find_primary_structures_of', :via => :get
  match "membranes/:id/lipids" => 'membranes#find_lipids', :via => :get

  match "species" => 'species#find_all', :via => :get
  match "species/:id" => 'species#find_one', :via => :get
  match "species/name/:name" => 'species#find_one_by_name', :via => :get
  match "species/:id/primary_structures" => 'species#find_primary_structures_of', :via => :get

  match "assemblies" => 'assemblies#find_all', :via => :get
  match "assemblies/:id" => 'assemblies#find_one', :via => :get
  match "assemblies/uniprot/:uniprotcode" => 'assemblies#find_one_by_uniprotcode', :via => :get
  match "assemblies/pdb/:pdbid" => 'assemblies#find_one_by_pdbid', :via => :get

  match "assembly_superfamilies" => 'assembly_superfamilies#find_all', :via => :get
  match "assembly_superfamilies/:id" => 'assembly_superfamilies#find_one', :via => :get
  match "assembly_superfamilies/:id/assembly_families" => 'assembly_superfamilies#find_families_of', :via => :get
  match "assembly_superfamilies/:id/assemblies" => 'assembly_superfamilies#find_assemblies_of', :via => :get

  match "assembly_families" => 'assembly_families#find_all', :via => :get
  match "assembly_families/:id" => 'assembly_families#find_one', :via => :get
  match "assembly_families/:id/assemblies" => 'assembly_families#find_assemblies_of', :via => :get

  match "assembly_membranes" => 'assembly_membranes#find_all', :via => :get
  match "assembly_membranes/:id" => 'assembly_membranes#find_one', :via => :get
  match "assembly_membranes/:id/assemblies" => 'assembly_membranes#find_assemblies_of', :via => :get


  ## Other one offs
  match "constants" => 'constants#find_by_names', :via => :get
  match "stats" => 'stats#basic_stats', :via => :get

  match "opm_update" => 'opm_update#opm_update', :via => :post
  match "opm_update/results/:id" => 'opm_update#results', :via => :get

  # match "fetch_pdb_info" => 'fetch_pdb_info#fetch_pdb_info', :via => :post
end
