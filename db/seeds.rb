# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Reset all Type Objects


# Type.delete_all
# Type.delete_all

require 'json'


#This assumes an import from a Json export
filename = "OPM-2018-08-10.json"
filepath = File.join Rails.root, "db", filename

old_tables = {}
current_table = ""
File.open(filepath).each do |line|
	if line[0..1] == "//"
		current_table = line[3...-2]
	elsif line[0] == "["
		old_tables[current_table] = JSON.parse(line)
		puts "Reading old table: " + current_table
	end
end

new_tables = {}
puts "Removing old tables and re-building with seed....\n\n"
Type.destroy_all
Classtype.destroy_all
Superfamily.destroy_all
Family.destroy_all
PrimaryStructure.destroy_all
StructureSubunit.destroy_all
SecondaryRepresentation.destroy_all
Species.destroy_all
Membrane.destroy_all
Citation.destroy_all


## ==================== create ID map ====================
id_map = {}

## ==================== creating new Type ====================
## ==================== creating new Type ====================
old_db_table = "tramaldom.type"
new_type = "Type"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

id_map[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")

	new_obj = Type.create(
		ordering: object["id"].to_f,
		name: object["name"].strip,
		description: object["description"],
		)
	new_tables[new_type][object["number"]] = new_obj

	id_map[new_type][new_obj.id] = object["number"]
end
puts :json=> Type.first
puts "Creating " +Type.all.count.to_s+ " of type '"+new_type+"'\n\n"


## ==================== creating new Classtype ====================
## ==================== creating new Classtype ====================
old_db_table = "tramaldom.class"
new_type = "Classtype"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

id_map[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	table_id = number[0..-2].join(".") + "."
	reftype = new_tables["Type"][table_id]

	new_obj = Classtype.create(
		ordering: old_id,
		name: object["name"],
		description: object["description"],
		type: reftype
		)
	new_tables[new_type][object["number"]] = new_obj

	id_map[new_type][new_obj.id] = object["number"]
end


puts :json=> Classtype.first
puts "Creating " +Classtype.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new SuperFamily ====================
## ==================== creating new SuperFamily ====================
old_db_table = "tramaldom.superfamily"
new_type = "Superfamily"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

id_map[new_type] = {}

for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	classtype_id = number[0..-2].join(".") + "."
	class_ref = new_tables["Classtype"][classtype_id]
	
	type_ref = new_tables["Type"][id_map["Type"][class_ref.type_id]]

	new_obj = Superfamily.create(
		ordering: old_id,
		name: object["name"],
		comment: object["comment"],
		tcdb: object["tcdb"],
		pfam: object["pfam"],
		type: type_ref,
		classtype: class_ref,
		)
	new_tables[new_type][object["number"]] = new_obj

	id_map[new_type][new_obj.id] = object["number"]

end
puts :json=> Superfamily.first
puts "Creating " +Superfamily.all.count.to_s+ " of type '"+new_type+"'\n\n"



## ==================== creating new Family ====================
## ==================== creating new Family ====================

old_db_table = "tramaldom.family"
new_type = "Family"
puts "Migrating " + old_db_table
new_tables[new_type] = {}


for object in old_tables[old_db_table] do
	number = object["number"].split(".")	
	old_id = number[-1]
	if number[-2].length == 2
		number[-2] = "0" + number[-2].to_s
	end
	parent_id = number[0..-2].join(".") + "."
	super_ref = new_tables["Superfamily"][parent_id]
	if super_ref == nil
		puts parent_id
		puts :json => super_ref
		exit()	
	end

	class_ref = new_tables["Classtype"][id_map["Classtype"][super_ref.classtype_id]]
	type_ref = new_tables["Type"][id_map["Type"][super_ref.type_id]]

	new_obj = Family.create(
		ordering: old_id,
		name: object["name"],
		description: "",
		tcdb: object["tcdb"],
		pfam: object["pfam"],
		superfamily: super_ref,
		classtype: class_ref,
		type: type_ref,
		)

	new_tables[new_type][object["id"]] = new_obj

end
puts :json=> Family.first
puts "Creating " +Family.all.count.to_s+ " of type '"+new_type+"'\n\n"


## ==================== creating new Species ====================
## ==================== creating new Species ====================

old_db_table = "tramaldom.species"
new_type = "Species"
puts "Migrating " + old_db_table

new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	new_obj = Species.create(
		ordering: object["id"],
		name: object["name"],
		name_common: "",
		)
	new_tables[new_type][object["id"]] = new_obj
end
puts :json=> Species.first
puts "Creating " +Species.all.count.to_s+ " of type '"+new_type+"'\n\n"


## ==================== creating new Membrane ====================
## ==================== creating new Membrane ====================
# Name, Short name, Topology In, Topology Out
membranes_topo = [
["Archaebacterial membrane","Archaebac.","cytoplasmic side ","extracellular side"],
["Bacterial Gram-negative inner membrane","Gram-neg. inner","cytoplasmic side","periplasm"],
["Bacterial Gram-negative outer membrane","Gram-neg. inner","periplasm","extracellular side"],
["Bacterial Gram-positive outer membrane","Gram-pos. outer","periplasm","extracellular side"],
["Bacterial Gram-positive plasma membrane","Gram-pos. inner","cytoplasmic side","periplasm"],
["Bacterial Gram-positive cell wall","Gram-pos. outer","periplasm","extracellular side"],
["Chloroplast inner membrane","Chloroplast inner","stroma","intermembrane space"],
["Chloroplast outer membrane","Chloroplast outer","cytoplasmic side","intermembrane space"],
["Cytoplasmic granule membrane","Granule","cytoplasmic side","intermembrane space"],
["Endoplasmic reticulum membrane","Endoplasm. reticulum","cytoplasmic side","lumenal side"],
["Endosome membrane","Endosome","cytoplasmic side","lumenal side"],
["Eukaryotic plasma membrane","Eykaryo. plasma","cytoplasmic side","extracellular side"],
["Golgi membrane","Golgi","cytoplasmic side","lumenal side"],
["Lysosome membrane","Lysosome","cytoplasmic side","lumenal side"],
["Mitochondrial inner membrane","Mitochon. inner","mitochondrial matrix","intermembrane space"],
["Mitochondrial outer membrane","Mitochon. outer","cytoplasmic side ","intermembrane space"],
["Nuclear inner membrane","Nuclear inner","nucleus matrix side","intermembrane space"],
["Nuclear outer membrane","Nuclear outer","cytoplasmic side","intermembrane space"],
["Peroxisome membrane","Peroxisome","cytoplasmic side","lumenal side"],
["Secreted","Secreted","inner side","outer side"],
["Thylakoid membrane","Thylakoid","stroma","thylakoid space"],
["Undefined","Undefined","inner side","outer side"],
["Vacuole membrane","Vacuole","cytoplasmic side","lumenal side"],
["Vesicle membrane","Vesicle","cytoplasmic side","lumenal side"],
["Viral membrane","Viral","intravirion side","virion surface"]
]
#given from lomize

membrane_hash = {}
for membrane in membranes_topo do
	membrane_hash[membrane[0]] = membrane
end

old_db_table = "tramaldom.membrane"
new_type = "Membrane"
puts "Migrating " + old_db_table

new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	mem_name = object["name"]
	mem = []
	if membrane_hash.key?(mem_name)
		mem = membrane_hash[object["name"]]
	else
		puts "<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>"
		puts "<<<<< NOT FOUND " + mem_name + " - CHECK HARDCODED seeds.rb >>>>>"
		puts "<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>"
		mem = ["","","",""]
	end

	new_obj = Membrane.create(
		ordering: object["id"],
		name: object["name"],
		short_name: mem[1],
		topology_in: mem[2],
		topology_out: mem[3],
		abbrevation: object["abbrevation"],
		)
	new_tables[new_type][object["id"]] = new_obj
end
puts :json=> Membrane.first
puts "Creating " +Membrane.all.count.to_s+ " of type '"+new_type+"'\n\n"


### =========================================
### =========================================
### =========================================
### =========================================
### BASIC OBJECTS ARE SETUP
### =========================================
### =========================================
### =========================================
### =========================================


## make groupings of complex structures
grouping = {} # primary_pdbid : [secondary pdbid]
old_db_table = "tramaldom.relatedproteins"
for relation in old_tables[old_db_table] do
	primary_pdbid = relation["protein_pdbid"].to_s
	secondary_pdbid = relation["pdbid"].to_s
	if !grouping.key?(primary_pdbid)
		grouping[primary_pdbid] = []
	end
	grouping[primary_pdbid].push(secondary_pdbid)

end

# puts grouping


## ==================== creating new PrimaryStructure ====================
## ==================== creating new PrimaryStructure ====================
old_db_table = "tramaldom.protein"
new_type = "PrimaryStructure"
puts "Migrating " + old_db_table + ".... this may take a few minutes"
new_tables[new_type] = {}

# puts old_tables[old_db_table][0]
# exit
structures = {} #pdb : structure

totalpro = 0
for object in old_tables[old_db_table] do
	pdbid = object["pdbid"]

	fam_ref = new_tables["Family"][object["family_id"]]
	
	super_ref = new_tables["Superfamily"][id_map["Superfamily"][fam_ref.superfamily_id]]
	class_ref = new_tables["Classtype"][id_map["Classtype"][fam_ref.classtype_id]]
	type_ref = new_tables["Type"][id_map["Type"][fam_ref.type_id]]


	topology_subunit = object["topology"].split(" ")[0]
	topology_show_in = false
	if object["topology"].split(" ")[1] == "in"
		topology_show_in = true
	end
	if object["topology"].split(" ")[0] == "in" or object["topology"].split(" ")[0] == "out"
		topology_subunit = ""
		topology_show_in = false
		if object["topology"].split(" ")[0] == "in"
			topology_show_in = true
		end
	end

	verification = object["verification"].gsub("\u00C3", "\u212B")
	#create primary grouping 
	primary = PrimaryStructure.create(
		ordering: new_tables[new_type].count,
		membrane: new_tables["Membrane"][object["membrane_id"]],
		species: new_tables["Species"][object["species_id"]],
		family: fam_ref,
		superfamily: super_ref,
		classtype: class_ref,
		type: type_ref,
		name: object["name"],
		comments: object["comments"],
		pdbid: object["pdbid"],
		resolution: object["resolution"],
		topology_show_in: topology_show_in,
		topology_subunit: topology_subunit,
		thickness: object["thickness"],
		thicknesserror: object["thicknesserror"],
		tilt: object["tilt"],
		tilterror: object["tilterror"],
		gibbs: object["gibbs"],
		tau: object["tau"],
		verification: verification
		)
	new_tables[new_type][object["id"]] = primary
	structures[primary.pdbid] = primary
	
	#create primary structure
	# puts :json=> object
	# puts :json=> new_tables["Family"][object["family_id"]]
	# puts :json=> structureGroup
	# primary.save
	relatedComplexes =  grouping[pdbid]
	if relatedComplexes == nil
		next
	end

	
	#create secondary structures
	number = 0
	for relatedComplexpdb in relatedComplexes do
		number += 1
		secondary = SecondaryRepresentation.create(
			ordering: number,
			pdbid: relatedComplexpdb,
			primary_structure: primary,
		)
		structures[secondary.pdbid] = secondary
	end
end
puts :json=> PrimaryStructure.first
puts :json=> SecondaryRepresentation.first
puts "Creating " +PrimaryStructure.all.count.to_s+ " of type '"+new_type+"' from "+ SecondaryRepresentation.all.count.to_s+" total complex structures\n\n"


## ==================== creating new Citations ====================
## ==================== creating new Citations ====================

old_db_table = "tramaldom.citation"
new_type = "Citations"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	primary_structure = new_tables["PrimaryStructure"][object["protein_id"]]
	new_obj = Citation.create(
		ordering: object["id"],
		name: "",
		pmid: object["pmid"],
		maintext: object["citation"].strip,
		primary_structure: primary_structure,
		)
	# new_tables[new_type][object["id"]] = new_obj
end
puts :json=> Citation.first
puts "Creating " +Citation.all.count.to_s+ " of type '"+new_type+"'\n\n"


## ==================== creating new StructureSubunits ====================
## ==================== creating new StructureSubunits ====================

old_db_table = "tramaldom.subunits"
new_type = "StructureSubunits"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do

	new_obj = StructureSubunit.create(
		ordering: object["id"],
		protein_letter: object["letter"],
		tilt: object["tilt"],
		segment: object["segment"],
		pdbid: object["pdbid"],
		segment_count: object["segment"].split(",").length
		)
	new_tables[new_type][object["id"]] = new_obj
end
puts :json=> StructureSubunit.first
puts "Creating " +StructureSubunit.all.count.to_s+ " of type '"+new_type+"'\n\n"

