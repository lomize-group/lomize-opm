class AddResolutionToSecondaryRepresentations < ActiveRecord::Migration[5.0]
    def change
        add_column :secondary_representations, :resolution, :string, default: ""
    end
end
