class CreateLipids < ActiveRecord::Migration[5.0]
	def change
		create_table :lipids do |t|
	    	t.boolean :leaflet_inner
	    	t.string :headgroup
	    	t.string :acyl1
	    	t.string :acyl2
	    	t.string :lipid
	    	t.integer :number_per_leaflet
	    	t.float :percentage
	    	t.string :membrane_name_cache, default: ""

	    	t.references :membrane, foreign_key: true, index: true

	    	t.timestamps
	    end
	end
end
