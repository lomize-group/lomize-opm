class CreateClasstypes < ActiveRecord::Migration[5.0]
  def change
    create_table :classtypes do |t|
      t.float :ordering
      t.string :name
      t.text :description
      t.references :type, foreign_key: true, index: true
      t.integer  :superfamilies_count, default: 0
      t.timestamps
    end
  end
end
