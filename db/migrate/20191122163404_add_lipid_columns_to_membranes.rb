class AddLipidColumnsToMembranes < ActiveRecord::Migration[5.0]
	def change
		add_column :membranes, :lipid_types_outer_leaflet_count, :integer, default: 0
		add_column :membranes, :lipid_types_inner_leaflet_count, :integer, default: 0
		add_column :membranes, :lipid_references, :text, default: ""
		add_column :membranes, :lipid_pubmed, :string, default: ""
	end
end
