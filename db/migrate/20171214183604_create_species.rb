class CreateSpecies < ActiveRecord::Migration[5.0]
  def change
    create_table :species do |t|
      t.float :ordering
      t.string :name
      t.string :name_common
      t.text :description
      t.integer  :primary_structures_count, default: 0

      t.timestamps
    end
  end
end
