class OpmUpdateAddJobHeartbeat < ActiveRecord::Migration[6.0]
    def change
        add_column :opm_update_files, :heartbeat, :datetime
    end
end
