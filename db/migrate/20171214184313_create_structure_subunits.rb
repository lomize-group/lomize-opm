class CreateStructureSubunits < ActiveRecord::Migration[5.0]
  def change
    create_table :structure_subunits do |t|
      t.float :ordering
      t.string :name
      t.text :description
      t.text :comments
      t.string :pdbid, index: true
      t.string :protein_letter
      t.string :uniprod_id1
      t.string :uniprod_id2
      t.string :uniprod_link
      t.string :tilt
      t.string :segment
      t.integer :segment_count
      t.timestamps
    end
  end
end
