class PrimaryStructuresInterproColumn < ActiveRecord::Migration[6.0]
  def change
      add_column :primary_structures, :interpro, :string
  end
end
