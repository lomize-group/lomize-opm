class CreateAssemblies < ActiveRecord::Migration[5.0]
	def change
		create_table :assemblies do |t|
			t.string :name
			t.string :pdbid, index: true
			t.string :protein_letter, index: true
			t.string :resolution
			t.string :uniprotcode

			t.text :aaseq

			t.string :transmembrane_segment
			t.integer :transmembrane_segment_count

			t.string :transmembrane_alpha_helix
			t.integer :transmembrane_alpha_helix_count

			t.string :other_alpha_helix
			t.integer :other_alpha_helix_count

			t.string :other_alpha_helix
			t.integer :other_alpha_helix_count

			t.string :beta_strand
			t.integer :beta_strand_count

			t.string :disordered_segment
			t.integer :disordered_segment_count

			t.string :helix_interaction
			t.string :equation

			t.string :primary_structure_name_cache, default: ""
			t.string :family_name_cache, default: ""
			t.string :superfamily_name_cache, default: ""
			t.string :membrane_name_cache, default: ""

			t.references :primary_structure, foreign_key: true, index: true
			t.references :family, foreign_key: true, index: true
			t.references :superfamily, foreign_key: true, index: true
			t.references :membrane, foreign_key: true, index: true

			t.timestamps
		end
	end
end
