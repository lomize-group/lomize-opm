class CreateFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :families do |t|
      t.float :ordering
      t.string :name
      t.text :description
      t.string :tcdb
      t.string :pfam
      t.references :type, foreign_key: true, index: true
      t.references :classtype, foreign_key: true, index: true
      t.references :superfamily, foreign_key: true, index: true
      t.integer  :primary_structures_count, default: 0
      t.timestamps
    end
  end
end
