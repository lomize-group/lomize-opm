class OpmUpdateFixPpmInpMatchesSubunit < ActiveRecord::Migration[6.0]
    def up
        # opm matches
        execute <<-SQL
            CREATE OR REPLACE VIEW opm_matches AS
            SELECT DISTINCT
                R.new_pdbid,
                R.pdbid,
                R.new_name,
                R.name,
                R.resolution,
                R.subunit,
                R.topology,
                R.membrane_id,
                R.species_id,
                R.family_id,
                R.all_uniprotcodes,
                R.uniprotcode_occurances,
                R.is_short,
                R.new_subunit
            FROM opm_match_result R
            WHERE R.pdbid IS NOT NULL
        SQL
    end

    def down
        # old version
        # opm matches
        execute <<-SQL
            CREATE VIEW opm_matches AS
            SELECT DISTINCT
                R.new_pdbid,
                R.pdbid,
                R.new_name,
                R.name,
                R.resolution,
                R.subunit,
                R.topology,
                R.membrane_id,
                R.species_id,
                R.family_id,
                R.all_uniprotcodes,
                R.uniprotcode_occurances
            FROM opm_match_result R
            WHERE R.pdbid IS NOT NULL
        SQL
    end
end
