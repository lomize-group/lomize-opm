class CreatePrimaryStructures < ActiveRecord::Migration[5.0]
  def change
    create_table :primary_structures do |t|
      t.float :ordering

      t.string :family_name_cache, default: ""
      t.string :species_name_cache, default: ""
      t.string :membrane_name_cache, default: ""

      t.string :name
      t.text :description
      t.text :comments
      t.string :pdbid, index: true
      t.string :resolution
      t.string :topology_subunit
      t.boolean :topology_show_in
      t.float :thickness
      t.float :thicknesserror
      t.integer :subunit_segments, default: 0
      t.integer :tilt
      t.integer :tilterror
      t.float :gibbs
      t.string :tau
      t.text :verification

      t.references :membrane, foreign_key: true, index: true
      t.references :species, foreign_key: true, index: true
      t.references :family, foreign_key: true, index: true
      t.references :superfamily, foreign_key: true, index: true
      t.references :classtype, foreign_key: true, index: true
      t.references :type, foreign_key: true, index: true

      t.integer  :secondary_representations_count, default: 0
      t.integer  :structure_subunits_count, default: 0
      t.integer  :citations_count, default: 0

      t.timestamps
    end
  end
end
