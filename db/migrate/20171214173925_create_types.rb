class CreateTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :types do |t|
      t.float  :ordering
      t.string   :name
      t.text     :description
      t.integer  :classtypes_count, default: 0
      t.timestamps
    end
  end
end
