class OpmUpdateAllUniprotcodesFix < ActiveRecord::Migration[6.0]
  def up
        execute "DROP TABLE opm_match_query CASCADE"

        # opm match query
        create_table :opm_match_query do |t|
            t.text :pdbid
            t.text :name
            t.text :resolution
            t.text :subunit
            t.text :match_uniprotcode
            t.text :all_uniprotcodes
            t.text :species
            t.text :lineage
            t.integer :membrane_id
            t.boolean :is_short
        end

        # opm update query interpro
        create_table :opm_update_query_interpro do |t|
            t.text :pdbid
            t.text :interpro
            t.text :family_name
        end

        # opm match result
        execute <<-SQL
            -- opm_match_result contains both matches and non matches,
            -- while opm_matches only contains matches,
            -- and opm_non_matches only contains non matches.
            CREATE VIEW opm_match_result AS
            -- build a proteins table with only one protein per (uniprotcode, family, topology) group
            WITH group_row_num AS (
                SELECT
                    ROW_NUMBER() OVER(PARTITION BY P0.family_id, P0.topology_show_in, U.uniprotcode ORDER BY P0.pdbid) AS group_row_num,
                    P0.pdbid,
                    P0.name,
                    P0.resolution,
                    P0.topology_subunit AS subunit,
                    P0.topology_show_in AS topology,
                    P0.membrane_id,
                    P0.species_id,
                    P0.family_id,
                    U.uniprotcode
                FROM primary_structures P0
                LEFT JOIN (
                        -- convert string-separated uniprot code list to its own table
                        SELECT
                            P1.id,
                            UNNEST(string_to_array(P1.uniprotcode, ' ')) AS uniprotcode
                        FROM primary_structures P1
                    ) U ON P0.id = U.id
            ),
            opm_match_proteins AS (
                SELECT
                    pdbid, name, resolution, subunit, topology, membrane_id, species_id, family_id, uniprotcode,
                    COUNT(*) OVER(PARTITION BY uniprotcode ORDER BY uniprotcode) AS uniprotcode_occurances
                FROM group_row_num
                WHERE group_row_num = 1
            )
            SELECT
                Q.pdbid AS new_pdbid,
                P.pdbid,
                Q.name AS new_name,
                P.name,
                P.resolution,
                P.subunit,
                P.topology,
                P.membrane_id,
                P.species_id,
                P.family_id,
                Q.match_uniprotcode,
                Q.all_uniprotcodes,
                Q.resolution AS new_resolution,
                Q.subunit AS new_subunit,
                Q.species AS new_species,
                Q.lineage AS new_lineage,
                Q.membrane_id AS new_membrane_id,
                Q.is_short,
                P.uniprotcode_occurances
            FROM opm_match_query Q
            -- perform uniprotcode search
            LEFT JOIN opm_match_proteins P ON Q.match_uniprotcode = P.uniprotcode
        SQL

        # opm matches
        execute <<-SQL
            CREATE VIEW opm_matches AS
            SELECT DISTINCT
                R.new_pdbid,
                R.pdbid,
                R.new_name,
                R.name,
                R.resolution,
                R.subunit,
                R.topology,
                R.membrane_id,
                R.species_id,
                R.family_id,
                R.all_uniprotcodes,
                R.uniprotcode_occurances
            FROM opm_match_result R
            WHERE R.pdbid IS NOT NULL
        SQL

        # opm non matches
        execute <<-SQL
            CREATE OR REPLACE VIEW opm_non_matches AS
            -- unlike opm_match_result, family_matches only includes the positive matches, not the entries that don't match
            WITH family_matches AS (
                SELECT DISTINCT
                    R0.new_pdbid,
                    F.id as family_id,
                    F.superfamily_id,
                    FT.topology,
                    FT.both_in_out
                FROM opm_match_result R0
                LEFT JOIN opm_update_query_interpro I ON R0.new_pdbid = I.pdbid
                JOIN families F ON I.interpro = F.interpro
                JOIN family_topology FT ON F.id = FT.id
                WHERE R0.pdbid IS NULL -- select non uniprot matched
                    -- make sure to exclude new_pdbids that have matches on other uniprot codes
                    AND R0.new_pdbid NOT IN (SELECT R1.new_pdbid FROM opm_matches R1)
            )
            SELECT
                R.new_pdbid,
                NULL AS pdbid,
                R.new_name,
                NULL AS name,
                R.new_resolution AS resolution,
                R.new_subunit AS subunit,
                F.topology,
                R.new_membrane_id AS membrane_id,
                S.id AS species_id,
                F.family_id,
                R.all_uniprotcodes,
                R.new_resolution,
                R.new_subunit,
                R.new_species,
                R.new_lineage,
                R.is_short,
                F.both_in_out,
                F.superfamily_id
            FROM opm_match_result R
            LEFT JOIN species S ON LOWER(R.new_species) = LOWER(S.name)
            LEFT JOIN family_matches F ON R.new_pdbid = F.new_pdbid
            -- make sure to exclude new_pdbids that have matches on other uniprot codes
            WHERE R.pdbid IS NULL
                AND R.new_pdbid NOT IN (SELECT R2.new_pdbid FROM opm_matches R2)
        SQL
  end

  # cannot downgrade
end
