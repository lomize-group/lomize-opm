class AddUniprotcodeToPrimaryStructures < ActiveRecord::Migration[5.0]
  def change
  	add_column :primary_structures, :uniprotcode, :string
  end
end
