class CreateSecondaryRepresentations < ActiveRecord::Migration[5.0]
  def change
    create_table :secondary_representations do |t|
      t.float :ordering
      t.string :pdbid, index: true
      t.string :name
      t.text :description
      t.text :comments
      t.references :primary_structure, foreign_key: true, index: true
      t.integer :structure_subunits_count
      t.integer :subunit_segments
      t.timestamps
    end
  end
end
