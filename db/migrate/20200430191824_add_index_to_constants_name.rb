class AddIndexToConstantsName < ActiveRecord::Migration[5.0]
  def change
    add_index :constants, :name, :unique => true
  end
end
