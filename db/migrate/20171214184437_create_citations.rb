class CreateCitations < ActiveRecord::Migration[5.0]
  def change
    create_table :citations do |t|
      t.float :ordering
      t.string :name
      t.text :maintext
      t.string :pmid
      t.text :comments
      t.references :primary_structure, foreign_key: true, index: true

      t.timestamps
    end
  end
end
