class CreateOpmUpdateFiles < ActiveRecord::Migration[6.0]
    def change
        create_table :opm_update_files do |t|
            t.string :filename
            t.string :content_type
            t.integer :status, default: 200
            t.binary :contents
            t.timestamps
        end
    end
end
