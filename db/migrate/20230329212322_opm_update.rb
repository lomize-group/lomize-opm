class OpmUpdate < ActiveRecord::Migration[6.0]
    def up
        # opm match query
        create_table :opm_match_query do |t|
            t.text :pdbid
            t.text :name
            t.text :resolution
            t.text :subunit
            t.text :uniprotcode
            t.text :interpro
            t.text :family_name
            t.text :species
            t.text :lineage
            t.integer :membrane_id
            t.boolean :is_short
        end

        # opm match result
        execute <<-SQL
            -- opm_match_result contains both matches and non matches,
            -- while opm_matches only contains matches,
            -- and opm_non_matches only contains non matches.
            CREATE OR REPLACE VIEW opm_match_result AS
            -- build a proteins table with only one protein per (uniprotcode, family, topology) group
            WITH group_row_num AS (
                SELECT
                    ROW_NUMBER() OVER(PARTITION BY P0.family_id, P0.topology_show_in, U.uniprotcode ORDER BY P0.pdbid) AS group_row_num,
                    P0.pdbid,
                    P0.name,
                    P0.resolution,
                    P0.topology_subunit AS subunit,
                    P0.topology_show_in AS topology,
                    P0.membrane_id,
                    P0.species_id,
                    P0.family_id,
                    U.uniprotcode
                FROM primary_structures P0
                LEFT JOIN (
                        -- convert string-separated uniprot code list to its own table
                        SELECT
                            P1.id,
                            UNNEST(string_to_array(P1.uniprotcode, ' ')) AS uniprotcode
                        FROM primary_structures P1
                    ) U ON P0.id = U.id
            ),
            opm_match_proteins AS (
                SELECT
                    pdbid, name, resolution, subunit, topology, membrane_id, species_id, family_id, uniprotcode,
                    COUNT(*) OVER(PARTITION BY uniprotcode ORDER BY uniprotcode) AS uniprotcode_occurances
                FROM group_row_num
                WHERE group_row_num = 1
            )
            SELECT
                Q.pdbid AS new_pdbid,
                P.pdbid,
                Q.name AS new_name,
                P.name,
                P.resolution,
                P.subunit,
                P.topology,
                P.membrane_id,
                P.species_id,
                P.family_id,
                Q.uniprotcode,
                Q.resolution AS new_resolution,
                Q.subunit AS new_subunit,
                Q.species AS new_species,
                Q.lineage AS new_lineage,
                Q.interpro AS new_interpro,
                Q.family_name AS new_family_name,
                Q.membrane_id AS new_membrane_id,
                Q.is_short,
                P.uniprotcode_occurances
            FROM OPM_MATCH_QUERY Q
            -- perform uniprotcode search
            LEFT JOIN opm_match_proteins P ON Q.uniprotcode = P.uniprotcode
        SQL

        # opm matches
        execute <<-SQL
            CREATE OR REPLACE VIEW opm_matches AS
            SELECT DISTINCT
                R.new_pdbid,
                R.pdbid,
                R.new_name,
                R.name,
                R.resolution,
                R.subunit,
                R.topology,
                R.membrane_id,
                R.species_id,
                R.family_id,
                R.uniprotcode,
                R.uniprotcode_occurances
            FROM opm_match_result R
            WHERE R.pdbid IS NOT NULL
        SQL

        # family topology
        execute <<-SQL
            -- get the majority topology for each family.
            -- each protein in a family determines its own topology.
            CREATE OR REPLACE VIEW family_topology AS
            WITH family_num_in_out AS (
                -- get the number of true/false for each family
                SELECT
                    F.id,
                    P.topology_show_in AS topology,
                    COUNT(*)
                FROM families F
                LEFT JOIN primary_structures P ON P.family_id = F.id
                GROUP BY F.id, P.topology_show_in
            )
            -- get majority topology bool for each family
            SELECT
                F.id,
                FIN.count AS in_count,
                FOUT.count AS out_count,
                CASE
                    -- if both NULL, then defaults to true.
                    -- not sure if this is the case for any family.
                    WHEN FOUT.count IS NULL THEN TRUE
                    WHEN FIN.count IS NULL THEN FALSE
                    -- if equal, default to true.
                    -- the end user will have to manually check topology.
                    WHEN FIN.count >= FOUT.count THEN TRUE
                    ELSE FALSE
                END AS topology,
                CASE
                    WHEN FIN.count IS NOT NULL AND FOUT.count IS NOT NULL THEN TRUE
                    ELSE FALSE
                END AS both_in_out
            FROM families F
            LEFT JOIN family_num_in_out FIN ON FIN.id = F.id AND FIN.topology
            LEFT JOIN family_num_in_out FOUT ON FOUT.id = F.id AND NOT FOUT.topology
        SQL

        # opm non matches
        execute <<-SQL
            CREATE OR REPLACE VIEW opm_non_matches AS
            WITH family_matches AS (
                SELECT
                DISTINCT
                    R0.new_pdbid,
                    R0.uniprotcode,
                    F.id as family_id,
                    F.superfamily_id,
                    FT.topology,
                    FT.both_in_out
                FROM opm_match_result R0
                JOIN families F ON R0.new_interpro = F.interpro
                JOIN family_topology FT ON F.id = FT.id
                WHERE R0.pdbid IS NULL
                    -- make sure to exclude new_pdbids that have matches on other uniprot codes
                    AND R0.new_pdbid NOT IN (SELECT R1.new_pdbid FROM opm_matches R1)
                ORDER BY
                    new_pdbid,
                    uniprotcode
            )
            SELECT
                R.new_pdbid,
                NULL AS pdbid,
                R.new_name,
                NULL AS name,
                R.new_resolution AS resolution,
                R.new_subunit AS subunit,
                F.topology,
                R.new_membrane_id AS membrane_id,
                S.id AS species_id,
                F.family_id,
                R.uniprotcode,
                R.new_resolution,
                R.new_subunit,
                R.new_species,
                R.new_lineage,
                R.new_interpro,
                R.new_family_name,
                R.is_short,
                F.both_in_out,
                F.superfamily_id
            FROM opm_match_result R
            LEFT JOIN species S ON LOWER(R.new_species) = LOWER(S.name)
            LEFT JOIN family_matches F ON R.new_pdbid = F.new_pdbid AND R.uniprotcode = F.uniprotcode
            -- make sure to exclude new_pdbids that have matches on other uniprot codes
            WHERE R.pdbid IS NULL
                AND R.new_pdbid NOT IN (SELECT R2.new_pdbid FROM opm_matches R2)
        SQL
    end

    def down
        execute "DROP VIEW opm_non_matches"
        execute "DROP VIEW family_topology"
        execute "DROP VIEW opm_matches"
        execute "DROP VIEW opm_match_result"
        execute "DROP TABLE opm_match_query"
    end
end
