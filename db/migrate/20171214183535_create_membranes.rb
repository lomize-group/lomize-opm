class CreateMembranes < ActiveRecord::Migration[5.0]
  def change
    create_table :membranes do |t|
      t.float :ordering
      t.string :name
      t.string :short_name
      t.string :topology_in
      t.string :topology_out
      t.text :description
      t.string :abbrevation
      t.integer  :primary_structures_count, default: 0
      
      t.timestamps
    end
  end
end
