class AddAssemblyCounterToFamiliesAndMembranes < ActiveRecord::Migration[5.0]
  def change
  	add_column :families, :assemblies_count, :integer, default: 0
  	add_column :membranes, :assemblies_count, :integer, default: 0
  end
end
