# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_05_04_231230) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "fuzzystrmatch"
  enable_extension "pg_stat_statements"
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "assemblies", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "pdbid"
    t.string "protein_letter"
    t.string "resolution"
    t.string "uniprotcode"
    t.text "aaseq"
    t.string "transmembrane_segment"
    t.integer "transmembrane_segment_count"
    t.string "transmembrane_alpha_helix"
    t.integer "transmembrane_alpha_helix_count"
    t.string "other_alpha_helix"
    t.integer "other_alpha_helix_count"
    t.string "beta_strand"
    t.integer "beta_strand_count"
    t.string "disordered_segment"
    t.integer "disordered_segment_count"
    t.string "helix_interaction"
    t.string "equation"
    t.string "primary_structure_name_cache", default: ""
    t.string "family_name_cache", default: ""
    t.string "superfamily_name_cache", default: ""
    t.string "membrane_name_cache", default: ""
    t.integer "primary_structure_id"
    t.integer "family_id"
    t.integer "superfamily_id"
    t.integer "membrane_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["family_id"], name: "index_assemblies_on_family_id"
    t.index ["membrane_id"], name: "index_assemblies_on_membrane_id"
    t.index ["pdbid"], name: "index_assemblies_on_pdbid"
    t.index ["primary_structure_id"], name: "index_assemblies_on_primary_structure_id"
    t.index ["protein_letter"], name: "index_assemblies_on_protein_letter"
    t.index ["superfamily_id"], name: "index_assemblies_on_superfamily_id"
  end

  create_table "citations", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "maintext"
    t.string "pmid"
    t.text "comments"
    t.integer "primary_structure_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["primary_structure_id"], name: "index_citations_on_primary_structure_id"
  end

  create_table "classtypes", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "description"
    t.integer "type_id"
    t.integer "superfamilies_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type_id"], name: "index_classtypes_on_type_id"
  end

  create_table "constants", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_constants_on_name", unique: true
  end

  create_table "families", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "description"
    t.string "tcdb"
    t.string "pfam"
    t.integer "type_id"
    t.integer "classtype_id"
    t.integer "superfamily_id"
    t.integer "primary_structures_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "assemblies_count", default: 0
    t.string "interpro"
    t.index ["classtype_id"], name: "index_families_on_classtype_id"
    t.index ["superfamily_id"], name: "index_families_on_superfamily_id"
    t.index ["type_id"], name: "index_families_on_type_id"
  end

  create_table "lipids", id: :serial, force: :cascade do |t|
    t.boolean "leaflet_inner"
    t.string "headgroup"
    t.string "acyl1"
    t.string "acyl2"
    t.string "lipid"
    t.integer "number_per_leaflet"
    t.float "percentage"
    t.string "membrane_name_cache", default: ""
    t.integer "membrane_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["membrane_id"], name: "index_lipids_on_membrane_id"
  end

  create_table "membranes", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.string "short_name"
    t.string "topology_in"
    t.string "topology_out"
    t.text "description"
    t.string "abbrevation"
    t.integer "primary_structures_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "assemblies_count", default: 0
    t.integer "lipid_types_outer_leaflet_count", default: 0
    t.integer "lipid_types_inner_leaflet_count", default: 0
    t.text "lipid_references", default: ""
    t.string "lipid_pubmed", default: ""
  end

  create_table "opm_match_query", force: :cascade do |t|
    t.text "pdbid"
    t.text "name"
    t.text "resolution"
    t.text "subunit"
    t.text "match_uniprotcode"
    t.text "all_uniprotcodes"
    t.text "species"
    t.text "lineage"
    t.integer "membrane_id"
    t.boolean "is_short"
  end

  create_table "opm_update_files", force: :cascade do |t|
    t.string "filename"
    t.string "content_type"
    t.integer "status", default: 200
    t.binary "contents"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "heartbeat"
  end

  create_table "opm_update_query_interpro", force: :cascade do |t|
    t.text "pdbid"
    t.text "interpro"
    t.text "family_name"
  end

  create_table "primary_structures", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "family_name_cache", default: ""
    t.string "species_name_cache", default: ""
    t.string "membrane_name_cache", default: ""
    t.string "name"
    t.text "description"
    t.text "comments"
    t.string "pdbid"
    t.string "resolution"
    t.string "topology_subunit"
    t.boolean "topology_show_in"
    t.float "thickness"
    t.float "thicknesserror"
    t.integer "subunit_segments", default: 0
    t.integer "tilt"
    t.integer "tilterror"
    t.float "gibbs"
    t.string "tau"
    t.text "verification"
    t.integer "membrane_id"
    t.integer "species_id"
    t.integer "family_id"
    t.integer "superfamily_id"
    t.integer "classtype_id"
    t.integer "type_id"
    t.integer "secondary_representations_count", default: 0
    t.integer "structure_subunits_count", default: 0
    t.integer "citations_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uniprotcode"
    t.string "interpro"
    t.index ["classtype_id"], name: "index_primary_structures_on_classtype_id"
    t.index ["family_id"], name: "index_primary_structures_on_family_id"
    t.index ["membrane_id"], name: "index_primary_structures_on_membrane_id"
    t.index ["pdbid"], name: "index_primary_structures_on_pdbid"
    t.index ["species_id"], name: "index_primary_structures_on_species_id"
    t.index ["superfamily_id"], name: "index_primary_structures_on_superfamily_id"
    t.index ["type_id"], name: "index_primary_structures_on_type_id"
  end

  create_table "secondary_representations", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "pdbid"
    t.string "name"
    t.text "description"
    t.text "comments"
    t.integer "primary_structure_id"
    t.integer "structure_subunits_count"
    t.integer "subunit_segments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "resolution", default: ""
    t.index ["pdbid"], name: "index_secondary_representations_on_pdbid"
    t.index ["primary_structure_id"], name: "index_secondary_representations_on_primary_structure_id"
  end

  create_table "species", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.string "name_common"
    t.text "description"
    t.integer "primary_structures_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "structure_subunits", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "description"
    t.text "comments"
    t.string "pdbid"
    t.string "protein_letter"
    t.string "uniprod_id1"
    t.string "uniprod_id2"
    t.string "uniprod_link"
    t.string "tilt"
    t.string "segment"
    t.integer "segment_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pdbid"], name: "index_structure_subunits_on_pdbid"
  end

  create_table "superfamilies", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "description"
    t.text "comment"
    t.string "tcdb"
    t.string "pfam"
    t.integer "type_id"
    t.integer "classtype_id"
    t.integer "families_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classtype_id"], name: "index_superfamilies_on_classtype_id"
    t.index ["type_id"], name: "index_superfamilies_on_type_id"
  end

  create_table "types", id: :serial, force: :cascade do |t|
    t.float "ordering"
    t.string "name"
    t.text "description"
    t.integer "classtypes_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "assemblies", "families"
  add_foreign_key "assemblies", "membranes"
  add_foreign_key "assemblies", "primary_structures"
  add_foreign_key "assemblies", "superfamilies"
  add_foreign_key "citations", "primary_structures"
  add_foreign_key "classtypes", "types"
  add_foreign_key "families", "classtypes"
  add_foreign_key "families", "superfamilies"
  add_foreign_key "families", "types"
  add_foreign_key "lipids", "membranes"
  add_foreign_key "primary_structures", "classtypes"
  add_foreign_key "primary_structures", "families"
  add_foreign_key "primary_structures", "membranes"
  add_foreign_key "primary_structures", "species"
  add_foreign_key "primary_structures", "superfamilies"
  add_foreign_key "primary_structures", "types"
  add_foreign_key "secondary_representations", "primary_structures"
  add_foreign_key "superfamilies", "classtypes"
  add_foreign_key "superfamilies", "types"
end
