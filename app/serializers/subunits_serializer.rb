include ActiveModel::Serialization
class SubunitsSerializer < ActiveModel::Serializer
  attributes :ordering, :name, :description, :comments, 
  		:protein_letter, :uniprod_id1, 
  		:uniprod_id2, :uniprod_link, :tilt, :segment, :assembly_id

  	def assembly_id
  		asm = object.assembly
  		asm ? asm.id : nil
  	end
end