include ActiveModel::Serialization
class SuperfamiliesSerializer < ActiveModel::Serializer
  attributes :id, :name, :pfam, :tcdb, :families_count

  has_one :classtype, serializer: ClasstypesSerializer
  has_many :families, serializer: FamiliesSerializer

  def classtype
  	object.classtype
  end

  def families
  	object.families
  end
  
end
