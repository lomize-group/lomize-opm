include ActiveModel::Serialization
class AssemblyMembranePageSerializer < ActiveModel::Serializer
   type :assembly_membranes
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: AssemblyMembraneSerializer

end
