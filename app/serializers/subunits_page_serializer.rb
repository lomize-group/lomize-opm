include ActiveModel::Serialization
class SubunitsPageSerializer < ActiveModel::Serializer
   type :subunits
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: SubunitsSerializer

end
