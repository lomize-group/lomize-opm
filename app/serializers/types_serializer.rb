include ActiveModel::Serialization
class TypesSerializer < ActiveModel::Serializer
  attributes :id, :name, :classtypes_count

  has_many :classtypes, serializer: ClasstypesSerializer

  def classtypes
  	object.classtypes
  end

end
