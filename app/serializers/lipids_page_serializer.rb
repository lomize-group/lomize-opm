include ActiveModel::Serialization
class LipidsPageSerializer < ActiveModel::Serializer
   type :lipids
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: LipidsSerializer

end
