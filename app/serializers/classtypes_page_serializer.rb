include ActiveModel::Serialization
class ClasstypesPageSerializer < ActiveModel::Serializer
  type :classtypes
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: ClasstypesSerializer

  
end
