include ActiveModel::Serialization
class AssemblyFamiliesSerializer < ActiveModel::Serializer
  attributes :id, :name, :pfam, :tcdb, :assemblies_count

  has_one :superfamily, serializer: SuperfamiliesSerializer
  has_many :assemblies, serializer: AssembliesSerializer

  def superfamily
  	object.superfamily
  end

  def assemblies
  	Assembly.where(:family_id => object.id)
  end

  def assemblies_count
  	Assembly.where(:family_id => object.id).count
  end

end
