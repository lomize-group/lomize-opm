include ActiveModel::Serialization
class SuperfamiliesPageSerializer < ActiveModel::Serializer
  type :superfamilies
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: SuperfamiliesSerializer

end
