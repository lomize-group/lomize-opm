include ActiveModel::Serialization
class CitationsSerializer < ActiveModel::Serializer
  attributes :id, :ordering, :name, :maintext, :pmid, :comments, :primary_structure_id

end
