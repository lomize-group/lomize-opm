include ActiveModel::Serialization
class CitationsPageSerializer < ActiveModel::Serializer
  type :citations
  attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: CitationsSerializer
    
end
