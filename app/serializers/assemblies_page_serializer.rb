include ActiveModel::Serialization
class AssembliesPageSerializer < ActiveModel::Serializer
   type :assemblies
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: AssembliesSerializer

end
