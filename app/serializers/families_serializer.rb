include ActiveModel::Serialization
class FamiliesSerializer < ActiveModel::Serializer
  attributes :id, :name, :pfam, :interpro, :tcdb, :primary_structures_count

  has_one :superfamily, serializer: SuperfamiliesSerializer
  has_many :primary_structures, serializer: PrimaryStructuresSerializer

  def superfamily
  	object.superfamily
  end

  def primary_structures
  	object.primary_structures
  end

end
