include ActiveModel::Serialization
class ClasstypesSerializer < ActiveModel::Serializer
  attributes :id, :name, :superfamilies_count

  has_one :type, serializer: TypesSerializer
  has_many :superfamilies, serializer: SuperfamiliesSerializer

  def type
  	object.type
  end

  def superfamilies
  	object.superfamilies
  end  

end
