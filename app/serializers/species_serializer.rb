include ActiveModel::Serialization
class SpeciesSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :primary_structures_count

  has_many :primary_structures, serializer: PrimaryStructuresSerializer
  
  def primary_structures
  	PrimaryStructure.where(:species_id => object.id)
  end

end
