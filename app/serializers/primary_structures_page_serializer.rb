include ActiveModel::Serialization
class PrimaryStructuresPageSerializer < ActiveModel::Serializer
   type :primary_structures
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: PrimaryStructuresSerializer

end
