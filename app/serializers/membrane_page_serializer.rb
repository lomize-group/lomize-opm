include ActiveModel::Serialization
class MembranePageSerializer < ActiveModel::Serializer
   type :membranes
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: MembraneSerializer
    

end
