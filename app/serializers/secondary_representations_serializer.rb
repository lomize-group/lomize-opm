include ActiveModel::Serialization
class SecondaryRepresentationsSerializer < ActiveModel::Serializer
	attributes :id, :pdbid, :resolution
end
