include ActiveModel::Serialization
class MembraneSerializer < ActiveModel::Serializer
  attributes :id, :name, :primary_structures_count, :short_name, :abbrevation,
  :topology_in, :topology_out, :lipid_references, :lipid_pubmed

  has_many :primary_structures, serializer: PrimaryStructuresSerializer
  
  def primary_structures
  	PrimaryStructure.where(:membrane_id => object.id)
  end

end
