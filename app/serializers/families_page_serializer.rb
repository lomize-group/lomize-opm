include ActiveModel::Serialization
class FamiliesPageSerializer < ActiveModel::Serializer
   type :families
   attributes :direction, :sort, :total_objects,
   :page_size, :page_start, :page_end, :page_num,
   :objects

   has_many :objects, serializer: FamiliesSerializer

end
