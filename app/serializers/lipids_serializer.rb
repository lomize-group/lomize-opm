include ActiveModel::Serialization
class LipidsSerializer < ActiveModel::Serializer
	attributes :id, :leaflet_inner, :headgroup, :acyl1, :acyl2, :lipid,
	:number_per_leaflet, :percentage, :membrane_name_cache, :membrane_id

	has_one :membrane, serializer: MembraneSerializer

	def membrane
		object.membrane
	end

end
