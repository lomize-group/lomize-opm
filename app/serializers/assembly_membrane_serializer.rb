include ActiveModel::Serialization
class AssemblyMembraneSerializer < ActiveModel::Serializer
  attributes :id, :name, :assemblies_count

  has_many :assemblies, serializer: AssembliesSerializer

  def assemblies
  	Assembly.where(:membrane_id => object.id)
  end

  def assemblies_count
  	Assembly.where(:membrane_id => object.id).count
  end

end
