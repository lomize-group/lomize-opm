include ActiveModel::Serialization
class PrimaryStructuresSerializer < ActiveModel::Serializer
    attributes :id, :ordering,
     	:pdbid,:name,:description,:comments,
     	:resolution,:topology_subunit, :topology_show_in,
      :thickness,:thicknesserror, :subunit_segments, :tilt, :tilterror, :gibbs, :tau, :verification, 
     	:family_name_cache,:species_name_cache,:membrane_name_cache,
      :membrane_id, :species_id, :family_id, :superfamily_id, :classtype_id, :type_id,
      :secondary_representations_count, :structure_subunits_count, :citations_count, :uniprotcodes


    has_one :family, serializer: FamiliesSerializer
    has_one :species, serializer: SpeciesSerializer
    has_one :membrane, serializer: MembraneSerializer

  	has_many :subunits, serializer: SubunitsSerializer
  	has_many :secondary_representations, serializer: SecondaryRepresentationsSerializer
  	has_many :citations, serializer: CitationsSerializer

  	def subunits
  		object.structure_subunits.order("protein_letter ASC")
  	end

  	def secondary_representations
		  object.secondary_representations.order("pdbid ASC")
	  end

  	def species
  		object.species
  	end

  	def membrane
  		object.membrane
  	end

  	def citations
  		object.citations
  	end

    def uniprotcodes
      (object.uniprotcode || "").split
    end

  	#kicks off the biological classification tree
  	def family
  		object.family
  	end

end
