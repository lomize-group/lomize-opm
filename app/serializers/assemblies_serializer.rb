include ActiveModel::Serialization
class AssembliesSerializer < ActiveModel::Serializer
    attributes :id, :name, :pdbid, :protein_letter, :resolution, :uniprotcode,
                :primary_structure_id, :family_id, :aaseq, :transmembrane_segment,
                :transmembrane_alpha_helix, :transmembrane_alpha_helix_count,
                :other_alpha_helix, :beta_strand, :disordered_segment,
                :helix_interaction, :equation, :subunit_segment, :subunit_segment_count,
                :superfamily_name_cache, :family_name_cache, :primary_structure_name_cache

    def subunit_segment
    	object.structure_subunit.segment
    end

    def subunit_segment_count
    	object.structure_subunit.segment_count
    end
end
