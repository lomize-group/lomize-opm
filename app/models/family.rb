class Family < ApplicationRecord
  
  belongs_to :type
  belongs_to :classtype
  validates :superfamily, presence: true
  belongs_to :superfamily, dependent: :destroy, counter_cache: true

  has_many :primary_structures, dependent: :destroy
  has_many :assemblies, dependent: :destroy
  
  def update_hierarchy
    parent = self.superfamily
    self.update(:type => parent.type, :classtype => parent.classtype)
  end

  def update_count
    Family.reset_counters(self.id, :primary_structures, :assemblies)
  end


  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name],
    using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":tsearch + :trigram"

end
