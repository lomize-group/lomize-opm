class ApplicationRecord < ActiveRecord::Base
	self.abstract_class = true
	def self.search(query)
		if query.present?
			search_full_text(query)
		else
			# No query? Return all records, no particular order.
			order("")
		end
	end

	def self.has_column?(sort_column)
		return self.column_names.include?(sort_column)
	end
end
