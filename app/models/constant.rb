class Constant < ApplicationRecord

	def self.get_each keys
		Constant.where("name IN (?)", keys).select(:id, :name, :value)
	end

	def self.get key
		Constant.find_by(:name => key).select(:id, :name, :value)
	end

	def self.put key, value
		c = Constant.find_by(:name => key) || Constant.new(:name => key)
		c.value = value
		c.save!
	end

end
