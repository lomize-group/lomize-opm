class StructureSubunit < ApplicationRecord
  belongs_to :primary_structure
  has_one :assembly

  def primary_structure
    return PrimaryStructure.where(:pdbid => self.pdbid).first
  end

  def assembly
    Assembly.find_by(:pdbid => self.pdbid, :protein_letter => self.protein_letter)
  end

  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name, :description, :comments, :protein_letter],
	using: {
      :trigram => {:threshold => 0.3},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"

end
