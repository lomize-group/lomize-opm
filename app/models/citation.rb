class Citation < ApplicationRecord
  belongs_to :primary_structure, counter_cache: true

  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name, :maintext, :pmid, :comments],
	using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"

end
