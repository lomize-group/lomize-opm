class Superfamily < ApplicationRecord
  
  belongs_to :type
  validates :classtype, presence: true
  belongs_to :classtype, dependent: :destroy, counter_cache: true

  has_many :families, dependent: :destroy
  
  def update_hierarchy
    parent = self.classtype
    self.update(:type => parent.type);
  end

  def update_count
    Superfamily.reset_counters(self.id, :families)
  end

  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name, :description, :comment],
  	using: {
      :trigram => {:threshold => 0.3},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"
  
end
