class Species < ApplicationRecord
  has_many :primary_structures

  def update_hierarchy

  end

  def update_count
    Species.reset_counters(self.id, :primary_structures)
  end

  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name, :name_common, :description],
    using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"

end
