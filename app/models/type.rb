class Type < ApplicationRecord
	include PgSearch::Model
  	has_many :classtypes, dependent: :destroy

  	def update_hierarchy
	    # probably dont need to do anything here
	    # method exists as a placeholder since type is the highest point in the hierarchy
      Type.reset_counters(self.id, :classtypes)
	  end

    def update_count
      # probably dont need to do anything here
      # method exists as a placeholder since type is the highest point in the hierarchy
      Type.reset_counters(self.id, :classtypes)
    end


  	pg_search_scope :search_full_text, against: [:name], 
  		using: {
          :trigram => {:threshold => 0.3},
          :tsearch => {:prefix => true}
        },
      :ranked_by => ":trigram"


end
