class OpmMatchQuery < ApplicationRecord
    self.table_name = "opm_match_query"

    belongs_to :membrane, dependent: :destroy, optional: true
end
