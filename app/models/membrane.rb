class Membrane < ApplicationRecord
	has_many :primary_structures
	has_many :assemblies
	has_many :lipids

	def update_hierarchy
		# do nothing
	end

	def update_count
		Membrane.reset_counters(self.id, :primary_structures, :assemblies)

		inner_count = self.lipids.where(:leaflet_inner => true).count
		outer_count = self.lipids.where(:leaflet_inner => false).count
		self.update(:lipid_types_inner_leaflet_count => inner_count, :lipid_types_outer_leaflet_count => outer_count)
	end

	include PgSearch::Model
	pg_search_scope :search_full_text,
	against: [:name, :description, :abbrevation],
  	using: {
  		:trigram => {:threshold => 0.30},
  		:tsearch => {:prefix => true}
  	},
  	:ranked_by => ":trigram"
end
