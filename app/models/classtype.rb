class Classtype < ApplicationRecord
  validates :type, presence: true
  belongs_to :type, dependent: :destroy, counter_cache: true
  
  has_many :superfamilies, dependent: :destroy

  def update_hierarchy
    #figure out ordering here....
    
  end

  def update_count
      # probably dont need to do anything here
      # method exists as a placeholder since type is the highest point in the hierarchy
      Classtype.reset_counters(self.id, :superfamilies)
  end

  include PgSearch::Model
  pg_search_scope :search_full_text, against: [:name, :description],
	using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    },
    :ranked_by => ":trigram"
end
