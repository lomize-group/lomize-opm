class Lipid < ApplicationRecord
	belongs_to :membrane, dependent: :destroy

	def update_hierarchy
		self.update_cache
	end

	def update_count
		# do nothing
	end

	def update_cache
		# just in case
		self.update_column(:membrane_name_cache, self.membrane.name)
	end
end
