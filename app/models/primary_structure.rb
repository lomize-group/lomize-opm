class PrimaryStructure < ApplicationRecord
  validates :family, presence: true

  has_many :citations, dependent: :destroy
  has_many :secondary_representations, dependent: :destroy
  has_many :structure_subunits
  
  def structure_subunits
    return StructureSubunit.where(:pdbid => self.pdbid)
  end

  belongs_to :membrane, dependent: :destroy, counter_cache: true
  belongs_to :species, dependent: :destroy, counter_cache: true
  belongs_to :family, dependent: :destroy, counter_cache: true
  belongs_to :superfamily
  belongs_to :classtype
  belongs_to :type

  def update_hierarchy
    parent = self.family
    self.update(:type => parent.type, :classtype => parent.classtype, :superfamily => parent.superfamily);
    self.update_cache
  end

  def update_cache
    self.update_column(:family_name_cache, self.family.name)
    self.update_column(:species_name_cache, self.species.name)
    self.update_column(:membrane_name_cache, self.membrane.short_name)
    
    subunit_segments = 0
    self.structure_subunits.find_each(:batch_size => 1000) do |segment|
        subunit_segments += segment.segment_count == nil ? 0 : segment.segment_count
    end
    self.update_column(:subunit_segments, subunit_segments);
    self.update_column(:structure_subunits_count, self.structure_subunits.count)
  end

  def update_count
    PrimaryStructure.reset_counters(self.id, :citations, :secondary_representations)
  end

  def self.search_full_text(query)
    union_query = query.gsub(' ', '').split(",")
    if union_query.count > 1
      search_full_text_any_impl(query)
    else
      search_full_text_prefix_impl(query)
    end
  end
  
  include PgSearch::Model
  pg_search_scope :search_full_text_any_impl,
    against: [:pdbid, :name],
    using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:any_word => true}
    }

  pg_search_scope :search_full_text_prefix_impl,
    against: [:pdbid, :name],
    using: {
      :trigram => {:threshold => 0.30},
      :tsearch => {:prefix => true}
    }

end
