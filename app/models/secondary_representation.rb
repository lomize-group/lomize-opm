class SecondaryRepresentation < ApplicationRecord
	belongs_to :primary_structure, dependent: :destroy, counter_cache: true

	include PgSearch::Model
	pg_search_scope :search_full_text,
	  against: [:pdbid],
	  # :associated_against => {
	  #     :secondary_representations => :pdbid
	  # },
	  using: {
	    :tsearch => {:any_word => true}
	  }
end
