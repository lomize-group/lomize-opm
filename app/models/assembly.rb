class Assembly < ApplicationRecord
	belongs_to :primary_structure, dependent: :destroy
	belongs_to :membrane, dependent: :destroy, counter_cache: true
	belongs_to :family, dependent: :destroy, counter_cache: true
	belongs_to :superfamily

	def structure_subunit
		StructureSubunit.find_by(:pdbid => self.pdbid, :protein_letter => self.protein_letter)
	end

	def primary_structure
		PrimaryStructure.find_by(:pdbid => self.pdbid)
	end

	def update_hierarchy
		protein = self.primary_structure
		if !protein.nil? && !self.structure_subunit.nil?
		  self.update(:primary_structure => protein, :family => protein.family,
		  				:superfamily => protein.superfamily, :membrane => protein.membrane)
		  self.update_cache
		end
	end

	def update_cache
		self.update_column(:resolution, self.primary_structure.resolution)
		self.update_column(:primary_structure_name_cache, self.primary_structure.name)
		self.update_column(:family_name_cache, self.family.name)
		self.update_column(:superfamily_name_cache, self.superfamily.name)
		self.update_column(:membrane_name_cache, self.membrane.short_name)
	end

	include PgSearch::Model
	pg_search_scope :search_full_text, against: 
		[:pdbid, :name, :family_name_cache, :resolution],
	  using: {
	    :trigram => {:threshold => 0.30},
	    :tsearch => {:prefix => true}
	  }

end
