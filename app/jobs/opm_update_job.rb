class OpmUpdateJob < ApplicationJob
    queue_as :default

    def perform id, pdb_list, sample_size, is_output_json
        OpmUpdate.do_update id, pdb_list, sample_size, is_output_json
    end
end
