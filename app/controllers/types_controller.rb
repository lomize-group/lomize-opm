class TypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	

	# match "types" => 'types#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Type.all.order(:id)
			file = file_response(Type, objects)
			return send_data file, :filename => "types-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		objects = Type.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Type.search(@search).count)
		render :json =>result, :status => 200, serializer: TypesPageSerializer
	end

	# match "types/:id" => 'types#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Type.find(@id)
		render :json =>result, :status => 200, serializer: TypesSerializer
	end

	# match "types/:id/classtypes" => 'types#find_classtypes_of', :via => :get
	def find_classtypes_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = Classtype.where(:type_id => @id).search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Classtype.where(:type_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: ClasstypesPageSerializer
	end

	# match "types/:id/primary_structures" => 'classtypes#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:type_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if that was not the sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PrimaryStructure.where(:type_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: PrimaryStructuresPageSerializer
	end
	
end