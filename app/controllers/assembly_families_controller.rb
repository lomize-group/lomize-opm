class AssemblyFamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "assembly_families" => 'assembly_families#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end
		families = Family.where("assemblies_count > 0").search(@search)
					.paginate(:page => @page, :per_page => @per_page)
					.order(@sort_column => @sort_direction)
		result = paged_response(families, families.count)
		render :json => result, :status => 200, serializer: AssemblyFamiliesPageSerializer
	end

	# match "assembly_families/:id" => 'assembly_families#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Family.find(@id)
		render :json => result, :status => 200, serializer: AssemblyFamiliesSerializer
	end


	# match "assembly_families/:id/assemblies" => 'assembly_families#find_assemblies_of', :via => :get
	def find_assemblies_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end

		objects = Assembly.where(:family_id => @id)

		# primary sort by sort column
		if Assembly.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary sort by id if that was not primary column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		# finally perform search and paginate
		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		
		result = paged_response(objects, objects.count)
		render :json => result, :status => 200, serializer: AssembliesPageSerializer
	end
end
