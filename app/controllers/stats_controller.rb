class StatsController < ApplicationController
	require 'ostruct'

	def structure_stats
		pdb_stats = []
		types = Type.all
		for type in types do
			classStats = []
			for classtype in type.classtypes.order("ordering ASC") do
				classStat = OpenStruct.new(
					:name => classtype.name,
					:id => classtype.id,
					:primary_structures_count => PrimaryStructure.where(:classtype_id => classtype.id).count,
					:secondary_representations_count => SecondaryRepresentation.joins(:primary_structure)
											.where(primary_structures: {:classtype_id => classtype.id}).all.count,
					)
				classStats.push(classStat)
			end
			typeStat = OpenStruct.new(
					:name => type.name,
					:id => type.id,
					:primary_structures_count => PrimaryStructure.where(:type_id => type.id).count,
					:secondary_representations_count => SecondaryRepresentation.joins(:primary_structure)
											.where(primary_structures: {:type_id => type.id}).all.count,
					:classes => classStats
			)
			pdb_stats.push(typeStat)
		end
		return pdb_stats
	end

	# match "stats" => 'stats#basic_stats', :via => :get
	def basic_stats
		response.headers['Access-Control-Allow-Origin'] = '*'
		
		pdb_stats = structure_stats()

		stats = OpenStruct.new(
			:types => Type.all.count,
			:classtypes => Classtype.all.count,
			:superfamilies => Superfamily.all.count,
			:family => Family.all.count,
			:primary_structure => PrimaryStructure.all.count,
			:species => Species.all.count,
			:membrane => Membrane.all.count,
			:structure_stats => pdb_stats,
			:assembly_superfamilies => Assembly.distinct.pluck(:superfamily_id).count,
			:assembly_families => Family.where("assemblies_count > 0").count,
			:assembly_membrane => Membrane.where("assemblies_count > 0").count,
			:assembly => Assembly.all.count
			)
		render :json =>stats, :status => 200
	end

end