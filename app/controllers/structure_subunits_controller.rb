class StructureSubunitsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "structure_subunits" => 'structure_subunits#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = StructureSubunit.all.order(:id)
			file = file_response(StructureSubunit, objects)
			return send_data file, :filename => "subunits-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		objects = StructureSubunit.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, StructureSubunit.search(@search).count)
		render :json =>result, :status => 200, serializer: SubunitsPageSerializer
	end

	# match "structure_subunits/pdbid/:pdbid" => 'structure_subunits#find_by_pdbid', :via => :get
	def find_by_pdbid
		response.headers['Access-Control-Allow-Origin'] = '*'
		pdbid = params[:pdbid] || ""

		# want exact pdbid match
		result = StructureSubunit.where(:pdbid => pdbid).order(:protein_letter)

		if result.count == 0
			secondary = SecondaryRepresentation.find_by(:pdbid => pdbid)
			if !secondary.nil?
				primary_pdbid = secondary.primary_structure.pdbid
				result = StructureSubunit.where(:pdbid => primary_pdbid).order(:protein_letter)
			end
		end

		render :json =>result, :status => 200, each_serializer: SubunitsSerializer
	end

	# match "structure_subunits/:id" => 'structure_subunits#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = StructureSubunit.where(:id => @id).first
		render :json =>result, :status => 200, serializer: SubunitsSerializer
	end



end