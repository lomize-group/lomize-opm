class CitationsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "citations" => 'citations#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Citation.all.order(:id)
			file = file_response(Citation, objects)
			return send_data file, :filename => "citations-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		objects = Citation.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Citation.search(@search).count)
		render :json =>result, serializer: CitationsPageSerializer, :status => 200
	end

	# match "citations/:id" => 'citations#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Citation.find(@id)
		render :json =>result, serializer: CitationsSerializer, :status => 200
	end

end