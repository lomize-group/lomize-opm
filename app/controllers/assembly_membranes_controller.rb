class AssemblyMembranesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "assembly_membranes" => 'assembly_membranes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end
		membranes = Membrane.where("assemblies_count > 0")
					.search(@search)
					.paginate(:page => @page, :per_page => @per_page)
					.order(@sort_column => @sort_direction)
		result = paged_response(membranes, membranes.count)
		render :json =>result, :status => 200, serializer: AssemblyMembranePageSerializer
	end

	# match "assembly_membranes/:id" => 'assembly_membranes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Membrane.find(@id)
		render :json =>result, :status => 200, serializer: AssemblyMembraneSerializer
	end


	# match "assembly_membranes/:id/assemblies" => 'assembly_membranes#find_assemblies_of', :via => :get
	def find_assemblies_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end

		objects = Assembly.where(:membrane_id => @id)

		# primary sort by sort column
		if Assembly.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary sort by id if that was not the first sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, objects.count)
		render :json =>result, :status => 200, serializer: AssembliesPageSerializer
	end
end
