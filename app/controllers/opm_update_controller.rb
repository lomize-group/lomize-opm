require 'csv'
require 'zip'

class OpmUpdateController < ApplicationController
    # match "opm_update/results/:id" => 'opm_update#results', :via => :get
    def results
        results_url = "/opm_update/results/#{params[:id]}"
        begin
            db_file = OpmUpdateFile.find(params[:id])
        rescue ActiveRecord::RecordNotFound
            status = 404
            json = {
                "id" => params[:id],
                "results" => results_url,
                "status" => status,
                "message" => "Not Found",
            }
            render :json => json, :status => status
            return
        end

        if db_file.contents.nil?
            status = 202
            json = {
                "id" => params[:id],
                "results" => results_url,
                "status" => status,
                "message" => "Accepted",
            }
            render :json => json, :status => status
            return
        end

        case db_file.content_type
        when "application/json"
            render :json => db_file.contents, :status => db_file.status
        when "application/zip"
            send_data db_file.contents,
                :type => "application/zip",
                :filename => db_file.filename,
                :dispotition => "attachment",
                :status => db_file.status
        else
            raise RuntimeError, "unexpected content type: #{db_file.content_type}"
        end
    end

    # match "opm_update" => 'opm_update#opm_update', :via => :post
    def opm_update
        db_file = OpmUpdateFile.new
        db_file.save
        results_url = "/opm_update/results/#{db_file.id}"
        status = 202
        json = {
            "id" => db_file.id,
            "results" => results_url,
            "status" => status,
            "message" => "Accepted",
        }
        render :json => json, :status => status

        if params.has_key? :pdb_list
            pdb_list = params[:pdb_list].read.strip.split
        else
            pdb_list = nil
        end

        if params.has_key? :random
            sample_size = !params[:random].nil? ? params[:random].to_i : 3
        else
            sample_size = nil
        end

        OpmUpdateJob.perform_later db_file.id, pdb_list, sample_size, (params.has_key? :json)
    end
end

module OpmUpdate
    def OpmUpdate.do_update id, pdb_list, sample_size, is_output_json
        db_file = OpmUpdateFile.find(id)
        results_url = "/opm_update/results/#{id}"

        # if pdb_list is not provided, it will operate on the query that's already in the db.
        # useful for debugging.
        if not pdb_list.nil? and not sample_size.nil?
            pdb_list = pdb_list.sample sample_size
        end

        if not pdb_list.nil?
            Rails.logger.debug "pdb list: " + pdb_list.join(" ")
            pdb_list = pdb_list.join "\n"
        end

        files = {
            "pdb-list.txt" => !pdb_list.nil? ? pdb_list : "",
            "download.sh" => DOWNLOAD_SH_FILE,
            "results-url.txt" => results_url,
        }

        if not pdb_list.nil?
            status, query_data, interpro_data, more_files = get_query_data pdb_list, id
            append_files files, more_files

            if status != 200
                upload_files db_file, files, is_output_json, status
                return
            end

            upload_query query_data, interpro_data
        end

        append_files files, get_matches
        append_files files, get_non_matches
        append_files files, get_new_species
        append_files files, get_new_families
        append_files files, get_ppm_input

        upload_files db_file, files, is_output_json
    end

    def OpmUpdate.get_ppm_input
        queries = []
        # non matches
        queries.append <<-SQL
            SELECT DISTINCT
                CASE
                    -- 'photosystems' super family
                    WHEN R.superfamily_id = 2 THEN TRUE
                    WHEN R.is_short THEN TRUE
                    ELSE false
                END AS include_ligands,
                R.topology,
                R.subunit,
                R.new_pdbid || '.pdb' AS pdbid
            FROM opm_non_matches R
            ORDER BY pdbid
        SQL

        # matches
        queries.append <<-SQL
            WITH get_row_num AS (
                SELECT
                    CASE
                        -- 'photosystems' super family
                        WHEN F.superfamily_id = 2 THEN TRUE
                        WHEN R.is_short THEN TRUE
                        ELSE false
                    END AS include_ligands,
                    R.topology,
                    R.new_subunit AS subunit,
                    R.new_pdbid || '.pdb' AS pdbid,
                    ROW_NUMBER() OVER (PARTITION BY R.new_pdbid) AS row_num
                FROM opm_matches R
                LEFT JOIN families F ON F.id = R.family_id
                ORDER BY pdbid
            )
            SELECT DISTINCT include_ligands, topology, subunit, pdbid
            FROM get_row_num
            WHERE row_num = 1 -- arbitrary selection but the user is notified with a warning
            ORDER BY pdbid
        SQL
        file_rows = []

        queries.each do |query|
            sql_rows = sql query
            file_rows += sql_rows.map { |row|
                values = [
                    row["include_ligands"] ? "1" : "0",
                    row["topology"].nil? ? "" : (row["topology"] ? "in" : "out"),
                    row["subunit"],
                    row["pdbid"],
                ]
                " %1s %-3s %s %8s" % values
            }
        end

        return { "ppm.inp" => file_rows.join("\n") }
    end

    def OpmUpdate.get_new_families
        txt_query = <<-SQL
            SELECT DISTINCT
                I.interpro,
                R.new_pdbid AS pdbid,
                R.new_species AS species,
                R.new_name AS name
            FROM opm_non_matches R
            LEFT JOIN opm_update_query_interpro I ON R.new_pdbid = I.pdbid
            WHERE R.family_id IS NULL -- select non family matched
            -- btw, include new entries that don't have interpro
            ORDER BY interpro, pdbid
        SQL
        csv_query = <<-SQL
            SELECT DISTINCT
                I.family_name AS name,
                NULL AS tcdb,
                NULL AS pfam,
                I.interpro,
                NULL AS type,
                NULL AS classtype,
                NULL AS superfamily
            FROM opm_non_matches R
            JOIN opm_update_query_interpro I ON R.new_pdbid = I.pdbid
            WHERE R.family_id IS NULL -- select non family matched
        SQL
        return {
            "new-families.txt" => sql_to_csv(sql txt_query),
            "new-families.csv" => sql_to_csv(sql csv_query),
        }
    end

    def OpmUpdate.get_new_species
        query = <<-SQL
            SELECT DISTINCT
                R.new_species AS species,
                R.new_lineage AS lineage
            FROM opm_non_matches R
            LEFT JOIN species S ON LOWER(R.new_species) = LOWER(S.name)
            WHERE R.new_species IS NOT NULL -- select new entries that have species
                AND S.id IS NULL    -- select non species matched
            ORDER BY species
        SQL
        return { "new-species.csv" => sql_to_csv(sql query) }
    end

    def OpmUpdate.get_non_matches
        files = {}
        query = <<-SQL
            SELECT
            DISTINCT
                new_pdbid,
                pdbid,
                new_name,
                name,
                resolution,
                subunit,
                topology,
                membrane_id,
                species_id,
                family_id,
                all_uniprotcodes
            FROM opm_non_matches
            ORDER BY new_pdbid, all_uniprotcodes, family_id, species_id
        SQL
        files["non-matches.csv"] = sql_to_csv(sql query)

        both_in_out_query = <<-SQL
            SELECT new_pdbid
            FROM opm_non_matches
            WHERE both_in_out
            GROUP BY new_pdbid
            ORDER BY new_pdbid
        SQL
        both_in_out_pdbs = (sql both_in_out_query).map { |row| row["new_pdbid"] }.join(" ")
        if not both_in_out_pdbs.empty?
            files["warnings.txt"] = "The family of these new unmatched entries have proteins with " \
                "both in and out topologies: #{both_in_out_pdbs}"
        end

        return files
    end

    def OpmUpdate.get_matches
        files = {}
        warnings = []
        matches_query = <<-SQL
            SELECT
                new_pdbid,
                pdbid,
                new_name,
                name,
                resolution,
                subunit,
                topology,
                membrane_id,
                species_id,
                family_id,
                all_uniprotcodes
            FROM opm_matches
            ORDER BY (CASE WHEN new_pdbid = pdbid THEN 1 ELSE 0 END),
                new_pdbid, pdbid, all_uniprotcodes, family_id, topology
        SQL
        files["matches.csv"] = sql_to_csv sql matches_query

        already_existing_query = <<-SQL
            SELECT
                R.new_pdbid
            FROM opm_matches R
            JOIN primary_structures P ON R.new_pdbid = P.pdbid
            WHERE R.pdbid IS NOT NULL
            GROUP BY R.new_pdbid
            ORDER BY R.new_pdbid
        SQL
        already_existing_pdbs = (sql already_existing_query).map { |row| row["pdbid"] }.join(" ")
        if not already_existing_pdbs.empty?
            warnings.append "These entries (matched or non matched) are already in opm: #{already_existing_pdbs}"
        end

        multiple_uniprot_query = <<-SQL
            SELECT new_pdbid
            FROM opm_matches
            WHERE uniprotcode_occurances > 1
            GROUP BY new_pdbid
            ORDER BY new_pdbid
        SQL
        multiple_uniprot_pdbs = (sql multiple_uniprot_query).map { |row| row["new_pdbid"] }.join(" ")
        if not multiple_uniprot_pdbs.empty?
            warnings.append "These new entries matched to existing entries that had different " \
                "properties (family or topology): #{multiple_uniprot_pdbs}"
        end

        if warnings
            files["warnings.txt"] = warnings.join("\n")
        end

        return files
    end

    def OpmUpdate.sql_to_csv sql_rows
        csv_rows = []
        csv = CSV.new(csv_rows)
        sql_rows.each do |row|
            csv << row.values
        end
        return csv_rows.join
    end

    def OpmUpdate.upload_query query_data, interpro_data
        sql "DELETE FROM opm_match_query"
        query_data.each_slice(10) do |rows|
            OpmMatchQuery.import rows
        end

        sql "DELETE FROM opm_update_query_interpro"
        interpro_data.each_slice(10) do |rows|
            Rails.logger.debug "interpro rows: #{rows.to_s}"
            OpmUpdateQueryInterpro.import rows
        end
    end

    def OpmUpdate.sql query
        return ActiveRecord::Base.connection.execute(query)
    end

    def OpmUpdate.get_query_data pdb_list, jobid
        files = {}

        query_data_csv, warnings, interpro_data_csv, exitok = run_opm_update_py(pdb_list, jobid)
        Rails.logger.debug "query_data.csv:\n#{query_data_csv}"
        Rails.logger.debug "interpro_data.csv:\n#{interpro_data_csv}"

        if not warnings.empty?
            files["warnings.txt"] = warnings
            Rails.logger.debug "warnings.txt: #{warnings}"
        end

        if not exitok
            message = "opm_update.py failed"
            files["query_data.csv"] = query_data_csv
            files["error.txt"] = message
            Rails.logger.debug "error.txt: #{message}"
            return 500, nil, nil, files
        end

        query_data = CSV.new(query_data_csv, :headers => :first_row).read
        retry_pdb = query_data.filter { |row| row[1] == "RETRY" }.map { |row| row["pdbid"] }.join(" ")
        query_data = query_data.filter { |row| row[1] != "RETRY" }.map { |row|
            row = row.to_h
            row["is_short"] = row["is_short"] == "True"
            row
        }

        if not retry_pdb.empty?
            if not files.has_key? "warnings.txt"
                files["warnings.txt"] = ""
            end
            files["warnings.txt"] += "Failed pdb codes: #{retry_pdb}"
        end

        interpro_data = CSV.new(interpro_data_csv, :headers => :first_row).read
        interpro_data = interpro_data.map { |row| row.to_h }
        return 200, query_data, interpro_data, files
    end

    def OpmUpdate.append_files files, more_files
        ensure_trailing_new_line files
        ensure_trailing_new_line more_files

        more_files.each do |filename, contents|
            if files.has_key? filename
                files[filename] += contents
            else
                files[filename] = contents
            end
        end
    end

    def OpmUpdate.upload_files db_file, files, is_output_json, status = 200
        ensure_trailing_new_line files
        results_dir_name = "opm-update-1a-results-#{db_file.id}"
        db_file.filename = results_dir_name
        db_file.status = status

        if is_output_json
            db_file.contents = files.to_json
            db_file.content_type = 'application/json'
            db_file.filename += ".json"
        else
            zip_data = Zip::OutputStream.write_buffer do |zio|
                files.each do |filename, contents|
                    zio.put_next_entry "#{results_dir_name}/#{filename}"
                    zio.write contents
                end
            end

            zip_data.rewind
            contents = zip_data.sysread
            db_file.contents = contents
            db_file.content_type = 'application/zip'
            db_file.filename += ".zip"
        end

        db_file.save
    end

    def OpmUpdate.ensure_trailing_new_line files
        files.each do |filename, contents|
            if not contents.empty? and not contents.ends_with? "\n"
                files[filename] = contents + "\n"
            end
        end
    end

    def OpmUpdate.run_opm_update_py(pdb_list, jobid)
        stdin_reader, stdin_writer = IO.pipe
        query_reader, query_writer = IO.pipe
        error_reader, error_writer = IO.pipe
        interpro_reader, interpro_writer = IO.pipe

        stdin_writer.write(pdb_list)
        stdin_writer.close
        pid = Process.spawn(
            "./opm_update.py --file - --interpro-fd #{interpro_writer.fileno}",
            :in => stdin_reader,
            :out => query_writer,
            :err => error_writer,
            interpro_writer.fileno => interpro_writer,
        )

        query_data_csv = ""

        counter = 0
        sleep_length = 5  # seconds
        timeout_time = 10 # minutes
        did_timeout = false

        while Process.waitpid(pid, Process::WNOHANG).nil?
            sql "UPDATE opm_update_files SET heartbeat = CURRENT_TIMESTAMP WHERE id = #{jobid}"
            sleep sleep_length
            new_output = ""

            # empty the pipe buffer
            while true
                begin
                    new_output += query_reader.read_nonblock 4096
                rescue Errno::EAGAIN
                    break
                rescue EOFError
                    Rails.logger.debug "./opm_update.py output: EOF error"
                    break
                end
            end

            if not new_output.empty?
                query_data_csv += new_output
                Rails.logger.debug "./opm_update.py output: " + new_output
                counter = 0
            else
                counter += sleep_length

                # time out x minutes after last output was collected
                if counter >= timeout_time * 60
                    Rails.logger.error "./opm_update.py timed out"
                    did_timeout = true
                    break
                end
            end
        end

        stdin_reader.close

        query_writer.close
        new_output = query_reader.read
        query_reader.close
        if not new_output.empty?
            Rails.logger.debug new_output
        end
        query_data_csv += new_output

        error_writer.close
        error_messages = error_reader.read
        error_reader.close

        if did_timeout
            error_messages += "\n./opm_update.py timed out, so not all data was collected from PDB\n"
        end

        interpro_writer.close
        interpro_data_csv = interpro_reader.read
        interpro_reader.close

        if not did_timeout
            exitok = $?.exitstatus == 0
        else
            # if timed out, pretend ok
            exitok = true
        end

        return query_data_csv, error_messages, interpro_data_csv, exitok
    end

    DOWNLOAD_SH_FILE = <<-EOF
#!/usr/bin/env bash
set -ex

DOWNLOAD_DIR="${1:-pdb-downloads}"
mkdir -p "${DOWNLOAD_DIR}"
PDB_LIST_FNAME="${2:-pdb-list.txt}"

function download-pdb () {
    PDBID="$1"

    if [ -z "$PDBID" ]; then
        echo "Must specify PDB ID"
        exit 1
    fi

    PDB_FNAME="${DOWNLOAD_DIR}/${PDBID}.pdb"
    CIF_FNAME="${DOWNLOAD_DIR}/${PDBID}.cif"
    PDB_URL="https://files.rcsb.org/download/${PDBID}.pdb1.gz"
    CIF_URL="https://files.rcsb.org/download/${PDBID}-assembly1.cif"

    if [ -a "$PDB_FNAME" ] || [ -a "$CIF_FNAME" ]; then
        echo "Already exists"
        return
    fi

    mkdir -p "${DOWNLOAD_DIR}"
    echo "Downloading ${PDBID} file"

    if curl --silent --head --fail "$PDB_URL" >/dev/null; then
        echo "Downloading PDB file"
        curl --fail "$PDB_URL" | gunzip > "$PDB_FNAME"
    elif curl --silent --head --fail "$CIF_URL" >/dev/null; then
        echo "Downloading CIF file"
        curl --fail "$CIF_URL" > "$CIF_FNAME"
    else
        echo "Neither PDB nor CIF file exist on RCSB"
        return 1
    fi
}

for pdb in $(cat "$PDB_LIST_FNAME"); do
    download-pdb "$pdb"
done
EOF
end
