class MembranesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "membranes" => 'membranes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Membrane.all.order(:id)
			file = file_response(Membrane, objects)
			return send_data file, :filename => "localizations-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end

		objects = Membrane.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Membrane.search(@search).count)
		render :json =>result, :status => 200, serializer: MembranePageSerializer
	end

	# match "membranes/:id" => 'membranes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Membrane.find(@id)
		render :json =>result, :status => 200, serializer: MembraneSerializer, include: ['']
	end

	# match "membranes/:id/primary_structures" => 'membranes#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:membrane_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if that was not the primary sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PrimaryStructure.where(:membrane_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: PrimaryStructuresPageSerializer,
		include: [
			'objects.secondary_representations'
		]
	end

    # this api sorts on the lipids table instead of primary_structures,
    # so the default sort column ("ordering") is not applicable
    def parse_sort_column_lipids
        @sort_column = "id"
        #sort
        if params[:sort] && params[:sort].to_s.length > 0
            @sort_column = params[:sort].to_s
        end
    end

	# match "membranes/:id/lipids" => 'membranes#find_lipids', :via => :get
	def find_lipids
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = Lipid.where(:membrane_id => @id)

		# allow filter by inner/outer leaflet
		if params[:leaflet] == "inner"
			objects = objects.where(:leaflet_inner => true)
		elsif params[:leaflet] == "outer"
			objects = objects.where(:leaflet_inner => false)
		end

        parse_sort_column_lipids

		# primary sort by column
		objects = objects.order(@sort_column => @sort_direction)

		# secondary order by id if that was not the primary sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, Lipid.where(:membrane_id => @id).search(@search).count)
		render :json => result, :status => 200, serializer: LipidsPageSerializer
	end

end
