class FamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "families" => 'families#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Family.all.order(:id)
			file = file_response(Family, objects)
			return send_data file, :filename => "families-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end

		objects = Family.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Family.search(@search).count)
		render :json => result, :status => 200, serializer: FamiliesPageSerializer
	end

	# match "families/:id" => 'families#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Family.find(@id)
		render :json => result, serializer: FamiliesSerializer, :status => 200, 
		include: [
		  'superfamily',
		  'superfamily.classtype',
		  'superfamily.classtype.type']
	end

	# match "families/pfamcode/:pfamcode" => 'families#find_by_pfam', :via => :get
	def find_by_pfam
		response.headers['Access-Control-Allow-Origin'] = '*'

		pfamcode = params[:pfamcode].downcase
		objects = Family.where("lower(pfam) = ?", pfamcode)
		@page = 1
		@per_page = objects.count
		result = paged_response(objects, @per_page)

		render :json => result, :status => 200, serializer: FamiliesPageSerializer
	end


	# match "families/:id/primary_structures" => 'families#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:family_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if that was not the sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, PrimaryStructure.where(:family_id => @id).search(@search).count)
		render :json => result, :status => 200, serializer: PrimaryStructuresPageSerializer,
		include: [
			'objects.secondary_representations'
		]
	end
  

end