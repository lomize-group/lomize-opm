class ClasstypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "classtypes" => 'classtypes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Classtype.all.order(:id)
			file = file_response(Classtype, objects)
			return send_data file, :filename => "classes-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		
		objects = Classtype.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Classtype.search(@search).count)
		render :json =>result, :status => 200, serializer: ClasstypesPageSerializer
	end

	# match "classtypes/:id" => 'classtypes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Classtype.find(@id)
		render :json =>result, :status => 200, serializer: ClasstypesSerializer
	end

	# match "classtypes/:id/superfamilies" => 'classtypes#find_superfamilies_of', :via => :get
	def find_superfamilies_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = Superfamily.where(:classtype_id => @id).search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Superfamily.where(:classtype_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: SuperfamiliesPageSerializer
	end

	# match "classtypes/:id/find_primary_structures_of" => 'classtypes#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:classtype_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			# sort by lowercase and trim (remove spaces) if the column type is string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if primary wasn't by id
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		# now perform search/paginate (ordering must be done first because search uses an order-by query)
		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)

		result = paged_response(objects, PrimaryStructure.where(:classtype_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: PrimaryStructuresPageSerializer
	end

end