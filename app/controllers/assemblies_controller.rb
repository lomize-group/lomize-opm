class AssembliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "assemblies" => 'assemblies#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Assembly.search(@search).order(:id)
			file = file_response(Assembly, objects)
			return send_data file, :filename => "assemblies-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		if @sort_column == "ordering"
			@sort_column = "id"
		end
		objects = Assembly.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Assembly.search(@search).count)
		render :json => result, :status => 200, serializer: AssembliesPageSerializer
	end

	# match "assemblies/uniprot/:uniprotcode" => 'assemblies#find_one_by_uniprot', :via => :get
	def find_one_by_uniprotcode
		response.headers['Access-Control-Allow-Origin'] = '*'
		uniprotcode = params[:uniprotcode]
		result = Assembly.find_by(:uniprotcode => uniprotcode)
		render :json => result, :status => 200, serializer: AssembliesSerializer
	end

	# match "assemblies/pdb/:pdbid" => 'assemblies#find_one_by_pdbid', :via => :get
	def find_one_by_pdbid
		response.headers['Access-Control-Allow-Origin'] = '*'
		pdbid = params[:pdbid]
		result = Assembly.find_by(:pdbid => pdbid)
		render :json => result, :status => 200, serializer: AssembliesSerializer
	end
	
	# match "assemblies/:id" => 'assemblies#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Assembly.find(@id)
		render :json => result, :status => 200, serializer: AssembliesSerializer
	end

end
