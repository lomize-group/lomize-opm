class SuperfamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "superfamilies" => 'superfamilies#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Superfamily.all.order(:id)
			file = file_response(Superfamily, objects)
			return send_data file, :filename => "superfamilies-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		objects = Superfamily.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Superfamily.search(@search).count)
		render :json =>result, :status => 200, serializer: SuperfamiliesPageSerializer
	end

	# match "superfamilies/:id" => 'superfamilies#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Superfamily.find(@id)
		render :json =>result, serializer: SuperfamiliesSerializer, :status => 200, 
		include: [
		  'classtype',
		  'classtype.type']
	end

	# match "superfamilies/:id/families" => 'superfamilies#find_families_of', :via => :get
	def find_families_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = Family.where(:superfamily_id => @id).search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Family.where(:superfamily_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: FamiliesPageSerializer
	end

	# match "superfamilies/:id/primary_structures" => 'classtypes#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:superfamily_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			# objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
			objects = objects.order(Arel.sql("lower(ltrim(#{PrimaryStructure.connection.quote_column_name(@sort_column)})) #{@sort_direction.upcase}"))
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if that was not the sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)

		result = paged_response(objects, PrimaryStructure.where(:superfamily_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: PrimaryStructuresPageSerializer,
		include: [
			"objects.primary_structure",
			"objects.species",
			"objects.membrane",
			"objects.family",
		]
	end
	
end