class AssemblySuperfamiliesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	# match "assembly_superfamilies" => 'assembly_superfamilies#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end
		superfamilies = Superfamily.where(:id => Family.where("assemblies_count > 0").distinct.pluck(:superfamily_id))
						.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(superfamilies, superfamilies.count)
		render :json =>result, :status => 200, serializer: SuperfamiliesPageSerializer
	end

	# match "assembly_superfamilies/:id" => 'assembly_superfamilies#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Superfamily.find(@id)
		render :json =>result, :status => 200, serializer: SuperfamiliesSerializer
	end

	# match "assembly_superfamilies/:id/families" => 'assembly_superfamilies#find_families_of', :via => :get
	def find_families_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		objects = Family.where(:superfamily_id => @id).where("assemblies_count > 0")
					.search(@search).paginate(:page => @page, :per_page => @per_page)
					.order(@sort_column => @sort_direction)
		result = paged_response(objects, Family.where(:superfamily_id => @id).search(@search).count)
		render :json =>result, :status => 200, serializer: AssemblyFamiliesPageSerializer
	end

	# match "assembly_superfamilies/:id/assemblies" => 'assembly_superfamilies#find_assemblies_of', :via => :get
	def find_assemblies_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @sort_column == "ordering"
			@sort_column = "id"
		end
		order = "#{@sort_column} #{@sort_direction}"
		if Assembly.type_for_attribute(@sort_column).type == :string
			order = "lower(ltrim(#{@sort_column})) #{@sort_direction}"
		end
		if @sort_column != 'id'
			order += ', id ' + @sort_direction
		end
		objects = Assembly.where(:superfamily_id => @id)
				.order(order)
				.search(@search)
				.paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, objects.count)
		render :json =>result, :status => 200, serializer: AssembliesPageSerializer
	end
end
