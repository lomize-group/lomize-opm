class ConstantsController < ApplicationController

	# match "constants" => 'constants#find_by_names', :via => :get
	def find_by_names
		response.headers['Access-Control-Allow-Origin'] = '*'

		constants_names = (params[:names] || "").split(",")
		if constants_names.count == 0
			result = Constant.all
		else
			result = Constant.get_each(constants_names)
		end
		
		result = {
			:objects => result.index_by(&:name)
		}

		render :json => result, :status => 200
	end
	
end
