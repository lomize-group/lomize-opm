class SecondaryRepresentationsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params

	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = SecondaryRepresentation.all.order(:id)
			file = file_response(SecondaryRepresentation, objects)
			return send_data file, :filename => "relatedPDB-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		search_query = SecondaryRepresentation.search(@search)
		objects = search_query.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, search_query.count)
		render :json => result, :status => 200
	end
end
