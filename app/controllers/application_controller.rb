class ApplicationController < ActionController::API
    require 'csv'
    require 'ostruct'
    include ActiveRecord::Sanitization
    include ActiveRecord::Sanitization::ClassMethods

    def cors_preflight_check
        if request.method == 'OPTIONS'
            cors_set_access_control_headers
            render plain: '', content_type: 'text/plain'
        end
    end

    def cors_set_access_control_headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, PATCH, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token, Auth-Token, Email, X-User-Token, X-User-Email'
        response.headers['Access-Control-Max-Age'] = '1728000'
    end

    def parse_paging_params_unsafe_strings
        #direction
        @sort_direction = "ASC"
        possible_directions = ["ASC", "DESC"]
        if params[:direction] && possible_directions.include?(params[:direction])
            @sort_direction = params[:direction]
        end
        @sort_direction = sanitize_sql_for_order(@sort_direction)

        #sort
        @sort_column = "ordering"
        if params[:sort] && params[:sort].to_s.length > 0
            @sort_column = params[:sort].to_s
            @sort_column = @sort_column.gsub('primary_structures', 'complex_structures')
        end
        @sort_column = sanitize_sql_for_order(@sort_column)

        #response format
        @fileFormat = ""
        possible_formats = ["csv"]
        if params[:fileFormat] && possible_formats.include?(params[:fileFormat].downcase)
            @fileFormat = params[:fileFormat].downcase
        end
    end

    def parse_paging_params
        parse_paging_params_unsafe_strings()

        @search = ""
        if params[:search]
            @search = params[:search].gsub('\'', '')
        end

        #pageSize
        @per_page = 50
        if params[:pageSize] && params[:pageSize].to_i > 0
            @per_page = params[:pageSize].to_i
        end
        
        #pageNum
        @page = 1
        if params[:pageNum] && params[:pageNum].to_i > 0
            @page = params[:pageNum].to_i
        end

        @id = 0
        if params[:id] && params[:id].to_i > 0
            @id = params[:id].to_i
        end
    end

    # def check_if_column_valid(target_models, column)
    #   # check that at least one of the models has the column
    #   target_models.each do |model|
    #       return true if model.has_column?(column)
    #   end
        
    #   # stop action execution and return 500 internal server error
    #   return head(500)
    # end

    def file_response(target_model, objects)
        headers = target_model.column_names
        CSV.generate(:headers => true) do |csv|
            csv << headers
            objects.each do |obj|
                csv << headers.map{ |col|
                    case when col == "pdbid"
                        then "=\"#{obj[col]}\""
                    else
                        obj[col]
                    end
                }
            end
        end
    end

    def paged_response(objects, total_objects)
        page_start = @per_page * (@page-1) + 1
        page_end = page_start + objects.length - 1
        if objects.length == 0
            page_start = 0
            page_end = 0
        end

        result = OpenStruct.new(
            :page_num => @page,
            :direction => "#{@sort_direction}",
            :sort => "#{@sort_column}",
            :total_objects => total_objects,
            :page_size => @per_page, 
            :page_start => page_start, 
            :page_end => page_end,
            :objects => objects)
        return result
    end

end
