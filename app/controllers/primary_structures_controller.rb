class PrimaryStructuresController < ApplicationController
	include ActiveRecord::Sanitization
	include ActiveRecord::Sanitization::ClassMethods
	
	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "primary_structures" => 'primary_structures#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = PrimaryStructure.all.order(:id)
			file = file_response(PrimaryStructure, objects)
			return send_data file, :filename => "proteins-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end

		primary_count = PrimaryStructure.search(@search).count
		secondary_count = SecondaryRepresentation.search(@search).count
		total_count = primary_count

		if @search != "" && secondary_count > 0
			total_count += secondary_count
			objects = PrimaryStructure.search(@search).paginate(:page => @page, :per_page => @per_page).to_a
			secondary = SecondaryRepresentation.search(@search).paginate(:page => @page, :per_page => @per_page)
			secondary.each do |s|
				s.primary_structure.pdbid = s.pdbid
				objects << s.primary_structure
			end

			# simplest way to get ordering correct
			if @sort_column != ""
				objects.sort!{|a,b| @sort_direction == "ASC" ? a[@sort_column] <=> b[@sort_column] : b[@sort_column] <=> a[@sort_column]}
			end
		else
			objects = nil

			# primary ordering by sort column
			if PrimaryStructure.type_for_attribute(@sort_column).type == :string
				objects = PrimaryStructure.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
			else
				objects = PrimaryStructure.order(@sort_column => @sort_direction)
			end

			# secondary ordering by id if that was not the primary sort column
			if @sort_column != 'id'
				objects = objects.order(:id => @sort_direction)
			end

			# finally perform search and paginate
			objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		end

		result = paged_response(objects, total_count)
		render :json =>result, :status => 200, serializer: PrimaryStructuresPageSerializer
	end

	# match "all_representations" => 'primary_structures#find_all_representations', :via => :get
	def find_all_representations
		response.headers['Access-Control-Allow-Origin'] = '*'
		secondary_cols = [
			'secondary_representations.*',
			'primary_structures.type_id',
			'primary_structures.classtype_id'
		].join(', ')

		objects = PrimaryStructure.all + SecondaryRepresentation.all
											.joins(:primary_structure)
											.select(secondary_cols)
		render :json => objects, :status => 200

	end
	
	# match "pdbids" => 'primary_structures#find_all_pdbids', :via => :get
	def find_all_pdbids
		response.headers['Access-Control-Allow-Origin'] = '*'

		 csv = []
		 objects = PrimaryStructure.select('pdbid')
		 for item in objects do
		 	csv.push(item.pdbid)
		 end

		 objects = SecondaryRepresentation.select('pdbid')
		 for item in objects do
		 	csv.push(item.pdbid)
		 end
		 
		 render :json => csv, :status => 200
	end

	# match "primary_structures/random" => 'primary_structures#find_random', :via => :get
	def find_random
		response.headers['Access-Control-Allow-Origin'] = '*'

		offset = rand(PrimaryStructure.count)
		result = PrimaryStructure.offset(offset).first

		render :json => result, serializer: PrimaryStructuresSerializer, :status => 200,
			include: [
				'subunits','secondary_representations', 'species', 'membrane', 'citations', 'family',
				'family.superfamily', 'family.superfamily.classtype', 'family.superfamily.classtype.type'
			]
	end

	# match "primary_structures/:id" => 'primary_structures#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'

		result = PrimaryStructure.find(@id)

		render :json => result, serializer: PrimaryStructuresSerializer, :status => 200,
			include: [
				'subunits', 'secondary_representations', 'species', 'membrane', 'citations',
				'family', 'family.superfamily', 'family.superfamily.classtype', 'family.superfamily.classtype.type'
			]
	end

    # this api sorts on the lipids table instead of primary_structures,
    # so the default sort column ("ordering") is not applicable
    def parse_sort_column_lipids
        @sort_column = "id"
        #sort
        if params[:sort] && params[:sort].to_s.length > 0
            @sort_column = params[:sort].to_s
        end
    end

	# match "primary_structures/:id/membrane_lipids" => 'primary_structures#find_membrane_lipids', :via => :get
	def find_membrane_lipids
		response.headers['Access-Control-Allow-Origin'] = '*'

		# get lipids for this protein's membrane
		membrane_id = PrimaryStructure.find(@id).membrane_id
		unordered_objects = Lipid.where(:membrane_id => membrane_id)

		# allow filter by inner/outer leaflet
		leaflet = params[:leaflet].to_s.downcase
		if leaflet == "inner"
			unordered_objects = unordered_objects.where(:leaflet_inner => true)
		elsif leaflet == "outer"
			unordered_objects = unordered_objects.where(:leaflet_inner => false)
		end

        parse_sort_column_lipids

		# primary sort by column
		objects = unordered_objects.order(@sort_column => @sort_direction)

		# secondary order by id if that was not the primary sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		result = paged_response(objects, unordered_objects.search(@search).count)
		render :json => result, :status => 200, serializer: LipidsPageSerializer	
	end
  	
  	# match "primary_structures/pdbid/:pdbid" => 'primary_structures#find_one_by_pdbid', :via => :get
  	def find_one_by_pdbid
  		response.headers['Access-Control-Allow-Origin'] = '*'

		pdbid = params[:pdbid]
		object = PrimaryStructure.find_by(:pdbid => pdbid)

		render :json => object, :serializer => PrimaryStructuresSerializer, :status => 200,
			include: [
				'subunits', 'secondary_representations', 'species', 'membrane', 'citations',
				'family', 'family.superfamily', 'family.superfamily.classtype','family.superfamily.classtype.type'
			]
	end

	# match "primary_structures/uniprotcode/:uniprotcode" => 'primary_structures#find_by_uniprotcode', :via => :get
	def find_by_uniprotcode
		response.headers['Access-Control-Allow-Origin'] = '*'

		# sanitize and format for SIMILAR TO statement.
		# ex: ABC_HUMAN,DEF_RAT -> ABC\_HUMAN|DEF\_RAT
		uniprotcodes = params[:uniprotcode].split(",").map{|u| sanitize_sql_like(u)}.join("|").downcase
		objects = PrimaryStructure.where("LOWER(uniprotcode) SIMILAR TO ?", "%(#{uniprotcodes})%")

		@page = 1
		@per_page = objects.count
		result = paged_response(objects, @per_page)

		render :json => result, :serializer => PrimaryStructuresPageSerializer, :status => 200
	end

	# match "primary_structures/pfamcode/:pfamcode" => 'primary_structures#find_by_pfam', :via => :get
	def find_by_pfam
		response.headers['Access-Control-Allow-Origin'] = '*'

		pfamcode = params[:pfamcode]
		objects = PrimaryStructure.joins(:family).where("families.pfam = ?", pfamcode)
		@page = 1
		@per_page = objects.count
		result = paged_response(objects, @per_page)

		render :json => result, :serializer => PrimaryStructuresPageSerializer, :status => 200
    end
    
    def get_pdb_file
        require 'net/http'
        response.headers['Access-Control-Allow-Origin'] = '*'

        pdbid = params[:pdbid]
        url = "https://files.rcsb.org/download/" << pdbid << ".pdb"
        response = Net::HTTP.get_response(URI(url))

        if response.code == "200"
            return render :json => {:pdbid => response.body}, :status => 200
        else
            return render :json => {:message => "File Not Found in PDB Database"}, :status => 404
        end
    end

	# match "primary_structures/advanced_search" => 'primary_structures#advanced_search', :via => :get
	def advanced_search
		response.headers['Access-Control-Allow-Origin'] = '*'

		# some functions to create consistency with formatting of input parameters
		def to_comparison cmp
			cmp = cmp.to_s
			operators = ["<", "<=", ">", ">=", "="]
			operators.include?(cmp) ? cmp : "<"
		end

		def int_or_nil num
			num.to_s == "" ? nil : num.to_i
		end

		def float_or_nil num
			num.to_s == "" ? nil : num.to_f
		end

		# get and sanitize string parameters.
		# the sanitize methods come from included modules at the top of this file
		# (ActiveRecord::Sanitization and ActiveRecord::Sanitization::ClassMethods)
		protein_name = sanitize_sql_like(params[:name].to_s.strip.downcase)
		pdbid = sanitize_sql_like(params[:pdbid].to_s.strip.downcase)
        uniprotcode = sanitize_sql_like(params[:uniprotcode].to_s.strip.downcase)
		type = sanitize_sql_like(params[:type].to_s.strip.downcase)
		classtype = sanitize_sql_like(params[:classtype].to_s.strip.downcase)
		species = sanitize_sql_like(params[:species].to_s.strip.downcase)
        taxon = sanitize_sql_like(params[:taxon].to_s.strip.downcase)
		membrane = sanitize_sql_like(params[:membrane].to_s.strip.downcase)
		superfamily = sanitize_sql_like(params[:superfamily].to_s.strip.downcase)
		family = sanitize_sql_like(params[:family].to_s.strip.downcase)

		# get and sanitize numerical and comparison parameters.
		# comparisons are strings that must be <, <=, >, >=, etc.
		resolution = float_or_nil(params[:resolution])
		resolution_compare = to_comparison(params[:resolution_compare])
		subunits_count = int_or_nil(params[:subunits_count])
		subunits_count_compare = to_comparison(params[:subunits_count_compare])
		subunit_segments_count = int_or_nil(params[:subunit_segments_count])
		subunit_segments_count_compare = to_comparison(params[:subunit_segments_count_compare])
		thickness = float_or_nil(params[:thickness])
		thickness_compare = to_comparison(params[:thickness_compare])
		tilt = int_or_nil(params[:tilt])
		tilt_compare = to_comparison(params[:tilt_compare])
		gibbs = float_or_nil(params[:gibbs])
		gibbs_compare = to_comparison(params[:gibbs_compare])

		objects = PrimaryStructure.all

		# run multiple WHERE clauses.
		# ActiveRecord will combine these together with "AND" if multiple are called.
		if protein_name != ""
			objects = objects.where("lower(primary_structures.name) LIKE ?", "%#{protein_name}%")
		end
		if pdbid != ""
			objects = objects.where("lower(primary_structures.pdbid) LIKE ?", "%#{pdbid}%")
		end
        if uniprotcode != ""
            objects = objects.where("lower(primary_structures.uniprotcode) LIKE ?", "%#{uniprotcode}%")
        end
		if type != ""
			objects = objects.joins(:type).where("lower(types.name) LIKE ?", "%#{type}%")
		end
		if classtype != ""
			objects = objects.joins(:classtype).where("lower(classtypes.name) LIKE ?", "%#{classtype}%")
		end
		if species != ""
			objects = objects.joins(:species).where("lower(species.name) LIKE ?", "%#{species}%")
		end
        if taxon != ""
            objects = objects.joins(:species).where("lower(species.description) LIKE ? ", "%#{taxon}%")
        end
		if membrane != ""
			objects = objects.joins(:membrane).where("lower(membranes.name) LIKE ?", "%#{membrane}%")
		end
		if superfamily != ""
			objects = objects.joins(:superfamily).where("lower(superfamilies.name) LIKE ?", "%#{superfamily}%")
		end
		if family != ""
			objects = objects.joins(:family).where("lower(families.name) LIKE ?", "%#{family}%")
		end

		# check that numeric values are non-nil before adding extra work for query.
		# check that compare is not empty string for sanity.
		if !resolution.nil? && resolution_compare != ""
			objects = objects.where("resolution #{resolution_compare} '#{resolution}'")
		end
		if !subunits_count.nil? && subunits_count_compare != ""
			objects = objects.where("structure_subunits_count #{subunits_count_compare} #{subunits_count}")
		end
		if !subunit_segments_count.nil? && subunit_segments_count_compare != ""
			objects = objects.where("subunit_segments #{subunit_segments_count_compare} #{subunit_segments_count}")
		end
		if !thickness.nil? && thickness_compare != ""
			objects = objects.where("thickness #{thickness_compare} #{thickness}")
		end
		if !tilt.nil? && tilt_compare != ""
			objects = objects.where("tilt #{tilt_compare} #{tilt}")
		end
		if !gibbs.nil? && gibbs_compare != ""
			objects = objects.where("gibbs #{gibbs_compare} #{gibbs}")
		end

		# get count before ordering because it's a waste to count the sorted results
		count = objects.count
		objects = objects.order(@sort_column => @sort_direction)

		result = paged_response(objects, count)
		render :json => result, :serializer => PrimaryStructuresPageSerializer, :status => 200
	end
  
end
