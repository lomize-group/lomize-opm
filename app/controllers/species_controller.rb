class SpeciesController < ApplicationController

	# declared in the ApplicationController
	before_action :parse_paging_params
	before_action :parse_paging_params_species

	def parse_paging_params_species
		@sort_column = "name"
		#sort
		if params[:sort] && params[:sort].to_s.length > 0
			@sort_column = params[:sort].to_s
		end
	end
	
	# match "species" => 'species#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Species.all.order(:id)
			file = file_response(Species, objects)
			return send_data file, :filename => "species-#{Date.today}.#{@fileFormat}", :type => @fileFormat
		end
		objects = Species.search(@search).paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Species.search(@search).count)
		render :json => result, :status => 200, serializer: SpeciesPageSerializer
	end

	# match "species/:id" => 'species#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Species.find(@id)
		render :json => result, :status => 200, serializer: SpeciesSerializer, include: ['']
	end

	# match "species/name/:name" => 'species#find_one_by_name', :via => :get
	def find_one_by_name
		response.headers['Access-Control-Allow-Origin'] = '*'
		species_name = params[:name].to_s.strip.downcase
		result = Species.find_by("lower(name) = ?", species_name)
		render :json => result, :status => 200, serializer: SpeciesSerializer, include: ['']
	end

	# match "species/:id/primary_structures" => 'classtypes#find_primary_structures_of', :via => :get
	def find_primary_structures_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = PrimaryStructure.where(:species_id => @id)

		# primary order by sort column
		if PrimaryStructure.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(ltrim(#{@sort_column})) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id if that was not the primary sort column
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		# finally search.paginate
		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)

		result = paged_response(objects, PrimaryStructure.where(:species_id => @id).search(@search).count)
		render :json => result, :status => 200, serializer: PrimaryStructuresPageSerializer,
		include: [
		  'objects.secondary_representations'
		]
	end

  

end
