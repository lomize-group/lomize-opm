#!/usr/bin/env python3
import contextlib
import csv
import functools
import logging
import os
import pprint
import random
import sys

import click
import requests

logging.basicConfig()
log = logging.getLogger(__name__)
stack_info = None

MEMBRANE_IDS = {
    'Archaebacterial membrane': 1,
    'Bacterial Gram-negative inner membrane': 2,
    'Bacterial Gram-negative outer membrane': 3,
    'Bacterial Gram-positive plasma membrane': 8,
    'Bacterial Gram-positive outer membrane': 19,
    'Eukaryotic plasma membrane': 4,
    'Endoplasmic reticulum membrane': 6,
    'Golgi membrane': 9,
    'Nuclear inner membrane': 10,
    'Nuclear outer membrane': 22,
    'Mitochondrial inner membrane': 5,
    'Mitochondrial outer membrane': 18,
    'Endosome membrane': 12,
    'Lysosome membrane': 21,
    'Peroxisome membrane': 11,
    'Vacuole membrane': 13,
    'Vesicle membrane': 14,
    'Cytoplasmic granule membrane': 24,
    'Thylakoid membrane': 7,
    'Chloroplast inner membrane': 16,
    'Chloroplast outer membrane': 17,
    'Viral membrane': 15,
    'Secreted': 20,
    'Undefined': 23,
    None: None,
}

# id -> accession
DELETED_UNIPROT = {
    # uniprot deleted this entry without cascading delete to 7f3x associated uniprot in entity #1
    'A0A1L1RNG8_CHICK': 'A0A1L1RNG8',
}


@click.command()
@click.option('--file', type=click.File(), default='pdb_input.txt', show_default=True)
@click.option('--interpro-fd', type=int)
@click.option('--random', 'selection_type', type=click.Choice(['all', 'sample', 'shuffle']), default='all', show_default=True)
@click.option('--sample-size', type=int, default=10, show_default=True)
@click.option('--pp', 'pp_output', is_flag=True)
@click.option('-v', 'verbose', is_flag=True)
@click.option('--debug', type=click.Choice(['log', 'raise', 'pdb', 'none']), default='log', show_default=True)
@click.argument('pdbs', nargs=-1, required=False)
def main(file, interpro_fd, selection_type, sample_size: int, pp_output: bool, verbose: bool, debug, pdbs):
    log_level = logging.WARNING
    if verbose:
        log_level = logging.DEBUG
    log.setLevel(log_level)
    global stack_info
    stack_info = log.level <= logging.DEBUG

    if not pdbs:
        pdbs = file.read().splitlines()

    match selection_type:
        case 'all':
            pass
        case 'sample':
            pdbs = random.sample(pdbs, k=sample_size)
            log.debug(f"random sample: {' '.join(pdbs)}")
        case 'shuffle':
            random.shuffle(pdbs)

    with get_interpro_writer(interpro_fd) as interpro_writer:
        query_data_csv_out = csv.writer(sys.stdout)
        interpro_data_csv_out = csv.writer(interpro_writer)

        if not pp_output:
            # header line
            query_data_csv_out.writerow([
                'pdbid',
                'name',
                'resolution',
                'subunit',
                'match_uniprotcode',
                'all_uniprotcodes',
                'species',
                'lineage',
                'membrane_id',
                'is_short',
            ])
            interpro_data_csv_out.writerow([
                'pdbid',
                'interpro',
                'family_name',
            ])

        for pdb in pdbs:
            pdb_data = process_new_pdb(pdb, debug)

            if pdb_data != 'RETRY':
                membrane = pdb_data['membrane']

                match len(membrane):
                    case 0:
                        membrane = None
                    case 1:
                        membrane = membrane[0]
                    case _:
                        log.warning(f'Multiple membrane types for {pdb}: {membrane.join(" ")}')
                        membrane = membrane[0]

            if pp_output:
                pprint.pprint({ pdb: pdb_data }, sort_dicts=False, width=128)
            else:
                if pdb_data == 'RETRY':
                    query_data_csv_out.writerow([
                        pdb,
                        'RETRY',
                    ])
                    sys.stdout.flush()
                    continue

                name = pdb_data['name']
                resolution = pdb_data['resolution']
                subunits = pdb_data['subunits']
                uniprot_codes = pdb_data['uniprot_codes']
                uniprot_code = pdb_data['uniprot_code']
                interpro = pdb_data['interpro']
                species = pdb_data['species']
                lineage = pdb_data['lineage']
                is_short = pdb_data['is_short']

                subunit = subunits[0] if subunits is not None else None

                if uniprot_code is not None:
                    uniprot_codes_2 = [uniprot_code]
                    all_uniprotcodes = ' '.join(sorted(uniprot_codes))
                elif uniprot_codes:
                    uniprot_codes_2 = sorted(uniprot_codes)
                    all_uniprotcodes = ' '.join(sorted(uniprot_codes))
                else:
                    uniprot_codes_2 = [None]
                    all_uniprotcodes = None

                for match_uniprotcode in uniprot_codes_2:
                    query_data_csv_out.writerow([
                        pdb,
                        name,
                        resolution,
                        subunit,
                        match_uniprotcode,
                        all_uniprotcodes,
                        species,
                        lineage,
                        MEMBRANE_IDS[membrane],
                        is_short,
                    ])

                if interpro:
                    for interpro, family_name in interpro.items():
                        interpro_data_csv_out.writerow([
                            pdb,
                            interpro,
                            family_name,
                        ])

            sys.stdout.flush()
            interpro_writer.flush()


@contextlib.contextmanager
def get_interpro_writer(interpro_fd):
    if interpro_fd is not None:
        interpro_reader = None
        interpro_writer = os.fdopen(interpro_fd, 'w')
    else:
        # for debugging, send interpro pipe to stdout
        fd_reader, fd_writer = os.pipe()
        interpro_reader = os.fdopen(fd_reader)
        interpro_writer = os.fdopen(fd_writer, 'w')

    try:
        yield interpro_writer
    finally:
        interpro_writer.close()

        if interpro_reader is not None:
            print(interpro_reader.read(), end='')
            interpro_reader.close()


def process_new_pdb(pdb: str, debug):
    def body():
        pdb_entry = get_pdb_entry(pdb)
        entities, is_short = get_entity_data(pdb, pdb_entry)
        uniprot_code, subunits = choose_uniprot_code(entities)
        uniprot_codes = get_all_uniprot_codes(entities)
        assert uniprot_code is None or len(uniprot_codes) > 0, \
            'chose uniprot code with no uniprot codes'

        if uniprot_code is not None:
            accession = uniprot_codes[uniprot_code]['accession']
            interpro = get_uniprot_protein_entry(accession)['interpro']
        else:
            interpro = None

        subunits, species, lineage, membrane = get_species(entities, uniprot_code, subunits, uniprot_codes)
        return {
            'entities': entities,
            'is_short': is_short,
            'name': pdb_entry['name'],
            'resolution': pdb_entry['resolution'],
            'uniprot_code': uniprot_code,
            'subunits': subunits,
            'uniprot_codes': uniprot_codes,
            'species': species,
            'lineage': lineage,
            'membrane': membrane,
            'interpro': interpro,
        }

    match debug:
        case 'none':
            return body()
        case _:
            try:
                return body()
            except Exception as err:
                match debug:
                    case 'log':
                        log.error(f'failure during {pdb}', exc_info=err)
                        return 'RETRY'
                    case 'raise':
                        raise err from Exception(f'failure during {pdb}')
                    case 'pdb':
                        log.error(f'failure during {pdb}', exc_info=err)
                        import pdb
                        pdb.post_mortem(err.__traceback__)
                        return 'RETRY'


def get_species(entities, uniprot_code, subunits, uniprot_codes):
    assert (uniprot_code is None) == (subunits is None), 'uniprot_code / subunits mismatch'

    if uniprot_code is not None:
        uniprot_data = uniprot_codes[uniprot_code]
        accession = uniprot_data['accession']
        topological_domain = uniprot_data['topological_domain']
        uniprot_protein_entry = get_uniprot_protein_entry(accession)
        species = uniprot_protein_entry['species']
        lineage = uniprot_protein_entry['lineage']
        membrane = get_membrane(lineage, topological_domain)
        return subunits, species, lineage, membrane

    # uniprot_code is None when there are zero transmembrane regions or there are no associated
    # uniprot codes.
    # take the species from the entity with the longest sequence length
    def sequence_length_sort(args):
        entity_id, entity_data = args
        return entity_data['sequence_length']

    sorted_entities = sorted(
        entities.items(),
        key=sequence_length_sort,
        reverse=True,  # choose max sequence length
    )

    assert len(sorted_entities) > 0, 'no entities'
    if len(sorted_entities) > 1:
        sequence_length_0 = sorted_entities[0][1]['sequence_length']
        sequence_length_1 = sorted_entities[1][1]['sequence_length']
        assert sequence_length_0 != sequence_length_1, \
            'need tie breaker in sequence length sort'

    entity_id, entity_data = sorted_entities[0]
    subunits = entity_data['subunits']

    if entity_data['uniprot']:
        (uniprot_code, uniprot_data), *more = entity_data['uniprot'].items()
        assert not more, \
            ('need tie breaker for uniprot code when selecting species for pdb entry with no '
             'transmemrane regions')
        accession = uniprot_data['accession']
        topological_domain = uniprot_data['topological_domain']
        uniprot_protein_entry = get_uniprot_protein_entry(accession)
        species = uniprot_protein_entry['species']
        lineage = uniprot_protein_entry['lineage']
        membrane = get_membrane(lineage, topological_domain)
        return subunits, species, lineage, membrane

    # no transmembrane regions and no uniprot codes
    assert len(entity_data['all_species']) == 1, \
        ('more than one species when selecting species for pdb entry with no transmembrane regions '
         'or uniprot codes')
    species, lineage_id = entity_data['all_species'][0]
    lineage = get_uniprot_lineage(lineage_id)
    membrane = get_membrane(lineage, topological_domain=None)
    return subunits, species, lineage, membrane


def get_membrane(lineage, topological_domain):
    classification = lineage.split(', ')[0].lower()

    match classification:
        case 'viruses':
            return ['Viral membrane']
        case 'archaea':
            return ['Archaebacterial membrane']
        case 'bacteria':
            domain_to_membrane = {
                'actinobacteria': 'Bacterial Gram-positive plasma membrane',
                'firmicutes': 'Bacterial Gram-positive plasma membrane',
                'thylakoid': 'Thylakoid membrane',
                'outer membrane': 'Bacterial Gram-positive outer membrane',
            }
        case 'eukaryota':
            domain_to_membrane = {
                'extracellular': 'Eukaryotic plasma membrane',
                'cell membrane': 'Eukaryotic plasma membrane',
                'endosome': 'Endosome membrane',
                'golgi apparatus': 'Golgi membrane',
                'secreted': 'Secreted',
                'endoplasmic reticulum': 'Endoplasmic reticulum membrane',
                'nucleus': 'Nuclear outer membrane',
                'mitochondrion inner membrane': 'Mitochondrial inner membrane',
                'mitochondrion outer membrane': 'Mitochondrial outer membrane',
                'lysosome': 'Lysosome membrane',
                'peroxisome': 'Peroxisome membrane',
                'vacuole': 'Vacuole membrane',
                'vesicle': 'Vesicle membrane',
                'thylakoid': 'Thylakoid membrane',
                'plastid inner membrane': 'Chloroplast inner membrane',
                'plastid outer membrane': 'Chloroplast outer membrane',
            }

    if topological_domain is None:
        return []

    membranes = [
        membrane
        for (domain, membrane) in domain_to_membrane.items()
        if any(domain in domain_2 for domain_2 in topological_domain)
    ]
    if not membranes and classification == 'bacteria':
        membranes.append('Bacterial Gram-negative inner membrane')
    return membranes


def get_uniprot_protein_entry(accession: str):
    """this gets the species for uniprot codes"""
    uniprot_protein_entry = web_json(f'https://www.ebi.ac.uk/proteins/api/proteins/{accession}')
    # all_species is a list of synonym species
    all_species = [
        get_first_two_words(name['value'])
        for name in uniprot_protein_entry['organism']['names']
    ]
    assert len(all_species) >= 1, 'not enough species'
    interpro = {
        entry['id']: entry['properties']['entry name']
        for entry in uniprot_protein_entry['dbReferences']
        if entry['type'] == 'InterPro'
    }
    return {
        'all_species': all_species,
        'species': get_first_two_words(all_species[0]),
        'lineage': ', '.join(uniprot_protein_entry['organism']['lineage']),
        'interpro': interpro,
    }


def get_uniprot_lineage(lineage_id: int):
    uniprot_lineage = web_json(f'https://www.ebi.ac.uk/proteins/api/taxonomy/lineage/{lineage_id}')
    return ', '.join(reversed([
        entry['scientificName']
        for entry in uniprot_lineage['taxonomies']
    ][1:-2]))  # chop off the first (the species name) and the last two ('root' & '... organisms')


def get_all_uniprot_codes(entities):
    return {
        uniprot_code: {
            'accession': uniprot_data['accession'],
            'topological_domain': uniprot_data['topological_domain'],
        }
        for entity_id, entity_data in entities.items()
        for uniprot_code, uniprot_data in entity_data['uniprot'].items()
    }


def choose_uniprot_code(entities):
    no_transmembrane_regions = all(
        uniprot_data['num_transmembrane_regions'] == 0
        for entity_id, entity_data in entities.items()
        for uniprot_code, uniprot_data in entity_data['uniprot'].items()
    )
    if no_transmembrane_regions:
        return None, None

    uniprot_code_to_num_transmembrane_regions = {}

    for entity_id, entity_data in entities.items():
        # step 1: sort uniprot codes by aligned region length
        if not entity_data['uniprot']:
            continue

        def aligned_region_length_sort(args):
            uniprot_code, uniprot_data = args
            return uniprot_data['aligned_region_length']

        sorted_uniprot_codes = sorted(
            entity_data['uniprot'].items(),
            key=aligned_region_length_sort,
            reverse=True, # choose max aligned region length
        )
        if len(entity_data['uniprot']) > 1:
            aligned_region_length_0 = sorted_uniprot_codes[0][1]['aligned_region_length']
            aligned_region_length_1 = sorted_uniprot_codes[1][1]['aligned_region_length']
            assert aligned_region_length_0 != aligned_region_length_1, \
                'need tie breaker in aligned region length sort'

        uniprot_code, uniprot_data = sorted_uniprot_codes[0]
        uniprot_code_to_num_transmembrane_regions[uniprot_code, entity_id] = \
            entity_data['uniprot'][uniprot_code]['num_transmembrane_regions']

    # step 2: sort the uniprot codes by min entity id and max num transmembrane regions,
    # taking advantage of stable sort (entity id is the tiebreaker)
    def entity_id_sort(args):
        (uniprot_code, entity_id), num_transmembrane_regions = args
        return entity_id

    def num_transmembrane_regions_sort(args):
        (uniprot_code, entity_id), num_transmembrane_regions = args
        return num_transmembrane_regions

    sorted_uniprot_codes = sorted(
        uniprot_code_to_num_transmembrane_regions.items(),
        key=entity_id_sort,
        # min entity id
    )
    sorted_uniprot_codes = sorted(
        sorted_uniprot_codes,
        key=num_transmembrane_regions_sort,
        reverse=True, # max num transmembrane regions
    )

    (uniprot_code, entity_id), num_transmembrane_regions = sorted_uniprot_codes[0]
    subunits = entities[entity_id]['subunits']
    return uniprot_code, subunits


def get_entity_data(pdb: str, pdb_entry):
    entity_ids = pdb_entry['entity_ids']
    entities = {
        entity_id: {
            **get_polymer_entity(pdb, entity_id),
            'uniprot':
                pdb_uniprot_entry
                if (pdb_uniprot_entry := get_pdb_uniprot_entry(pdb, entity_id)) is not None
                else {},
        }
        for entity_id in entity_ids
    }

    for entity_id, entity_data in entities.items():
        # reorganize aligned region length into uniprot code data
        aligned_regions = entity_data['aligned_regions']
        if len(entity_data['uniprot']) != len(aligned_regions):
            # this may be because a uniprot code was deleted from uniprot, but the aligned region
            # information remains in pdb
            log.debug('length mismatch between uniprot codes and aligned regions', stack_info=stack_info)
        for uniprot_data in entity_data['uniprot'].values():
            uniprot_data['aligned_region_length'] = aligned_regions[uniprot_data['accession']]

    # is short if all subunits have length less than 25
    is_short = all(
        entity_data['sequence_length'] < 25
        for entity_data in entities.values()
    )
    return entities, is_short


def get_pdb_entry(pdb: str, expect=True):
    # only return what is needed
    pdb_entry = web_json(f'https://data.rcsb.org/rest/v1/core/entry/{pdb}')

    # resolution logic taken from backend update script
    methods = {
        'X-RAY DIFFRACTION': None,
        'ELECTRON MICROSCOPY': 'EM',
        'ELECTRON CRYSTALLOGRAPHY': 'EC',
        'NEUTRON DIFFRACTION': 'NC',
        'FIBER DIFFRACTION': 'FD',
        'SOLUTION NMR': 'NMR',
    }

    try:
        method = pdb_entry['exptl'][0]['method']
        method = methods[method]
    except (KeyError, IndexError):
        method = 'N/A'

    match method:
        case None | 'EM' | 'EC' | 'NC' | 'FD':
            try:
                resolution = pdb_entry['rcsb_entry_info']['resolution_combined'][0]
                resolution = round(resolution, ndigits=2)
            except (KeyError, TypeError, ValueError):
                resolution = 'N/A'
            else:
                if method is not None:
                    resolution = f'{resolution} {method}'
        case 'NMR':
            resolution = 'NMR'
        case _:
            resolution = 'N/A'

    return {
        'entity_ids': pdb_entry['rcsb_entry_container_identifiers']['polymer_entity_ids'],
        'name': pdb_entry['struct']['title'],
        'resolution': resolution,
    }


def get_pdb_uniprot_entry(pdb: str, entity_id: str):
    # only return what's needed
    pdb_uniprot_entry = web_json(
        f'https://data.rcsb.org/rest/v1/core/uniprot/{pdb}/{entity_id}',
        expect=False,
    )
    # "no data" can be represented by HTTP 404 or json [None]
    if pdb_uniprot_entry is None or pdb_uniprot_entry == [None]:
        return None
    uniprot_codes = [
        uniprot_code
        for uniprot_data in pdb_uniprot_entry
        if (uniprot_code := uniprot_data['rcsb_uniprot_entry_name'][0]) not in DELETED_UNIPROT
    ]
    assert len(uniprot_codes) == len(set(uniprot_codes)), 'duplicate uniprot codes'
    pdb_uniprot_entry_2 = {}

    for uniprot_data in pdb_uniprot_entry:
        uniprot_code = uniprot_data['rcsb_uniprot_entry_name'][0]
        if uniprot_code in DELETED_UNIPROT:
            continue
        accession = uniprot_data['rcsb_id']
        uniprot_features = uniprot_data.get('rcsb_uniprot_feature', [])
        num_transmembrane_regions = sum(
            1
            for feature in uniprot_features
            if feature['type'] == 'TRANSMEMBRANE_REGION'
        )
        topological_domain = {
            feature['description'].lower()
            for feature in uniprot_features
            if feature['type'] == 'TOPOLOGICAL_DOMAIN'
        }
        # to find 'secreted' localization
        for feature in uniprot_features:
            if feature['type'] == 'CHAIN' and (description := feature.get('description')) is not None:
                topological_domain.add(description.lower())
        uniprot_keyword = [
            keyword['value'].lower()
            for keyword in uniprot_data.get('rcsb_uniprot_keyword', {})
        ]
        if 'secreted' in uniprot_keyword:
            topological_domain.add('secreted')
        pdb_uniprot_entry_2[uniprot_code] = {
            'accession': accession,
            'num_transmembrane_regions': num_transmembrane_regions,
            'topological_domain': topological_domain,
        }

    if len(pdb_uniprot_entry_2) > 1:
        log.warning(f'Hybrid sequence detected for {pdb}/{entity_id}')
    return pdb_uniprot_entry_2


def get_polymer_entity(pdb: str, entity_id: str):
    """among other things, this gets the species for PDB entries with no uniprot codes"""
    # organize the data based on how we'll use it
    # and only return what's needed
    polymer_entity = web_json(f'https://data.rcsb.org/rest/v1/core/polymer_entity/{pdb}/{entity_id}')
    aligned_regions = {}

    # some entities don't have aligned region data, because they don't have any uniprot codes
    for aligned_regions_2 in polymer_entity.get('rcsb_polymer_entity_align', []):
        accession = aligned_regions_2['reference_database_accession']
        if accession in DELETED_UNIPROT.values():
            continue
        length = sum(
            aligned_regions_3['length']
            for aligned_regions_3 in aligned_regions_2['aligned_regions']
        )
        aligned_regions[accession] = length

    subunits = polymer_entity['rcsb_polymer_entity_container_identifiers']['auth_asym_ids']
    sequence_length = polymer_entity['entity_poly']['rcsb_sample_sequence_length']
    # TODO: 1smz/1 does not have scientific name
    all_species = []

    for organism in polymer_entity['rcsb_entity_source_organism']:
        scientific_name = organism.get('ncbi_scientific_name')
        if scientific_name is None:
            scientific_name = organism['scientific_name']
        scientific_name = get_first_two_words(scientific_name)
        all_species.append((scientific_name, organism['ncbi_taxonomy_id']))

    return {
        'aligned_regions': aligned_regions,
        'subunits': subunits,
        'sequence_length': sequence_length,
        'all_species': all_species,
    }


def get_first_two_words(sentence: str):
    return ' '.join(sentence.split()[:2])


@functools.lru_cache
def web_json(url, *, expect=True):
    r = requests.get(url)
    if expect:
        r.raise_for_status()
    return r.json() if r else None


if __name__ == '__main__':
    main()
