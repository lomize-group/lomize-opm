import pdb
import requests

SPEC_ENDS = {
"primary_structures/pdbid/": '7l1e',
"species/name/": 'Guillardia%20theta',
"assemblies/uniprot/": 'Q96A54',
"assemblies/pdb/": '5lxg',
}

if __name__ == "__main__":
    f = open("config/routes.rb",'r')
    o_f = open("endpoint_test_output.txt","w")
    endpoint_lines = [l.lstrip().split(" ")[1].replace("\"","") for l in f.read().split("\n") if 'match' in l]
    print("===\nNow running all endpoints\n===\n", file = o_f)

    failed_endpoints = []

    base_url = "https://postgres-13-lomize-group-opm.herokuapp.com"
    # base_url = "http://0.0.0.0:3000"
    for url in endpoint_lines:
        try:
            to_replace = url.index(":")
            try:
                next_slash = url.index("/",to_replace)
                next_portion = url[next_slash:]
            except:
                next_portion = ""
            if url[0:to_replace] in SPEC_ENDS:
                url = f'{url[0:to_replace]}{SPEC_ENDS[ url[0:to_replace] ]}'
            else:
                url = url[0:to_replace]+"1"+next_portion
        except:
            pass
        print("\n===Running Endpoint " + url + "===", file=o_f)

        req = requests.get(base_url+"/"+url)
        data = req.json()
        if req.status_code == 500:
            failed_endpoints.append(url)
        print(data, file = o_f)
    
    if len(failed_endpoints) > 0:
        print("WARNING: The following endpoints failed: ")
        for url in failed_endpoints:
            print(url)
