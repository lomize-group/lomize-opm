import requests
if __name__ == "__main__":
    total_objs = 6000
    base_url = "http://0.0.0.0:3000/primary_structures/"
    for i in range(total_objs):
        actual_url = base_url + str(i) +"/membrane_lipids"
        r = requests.get(actual_url)
        if r.status_code != 500:
            print(r)